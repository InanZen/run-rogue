﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RunRogue
{
    public class AStarSquare
    {
        public Point P;
        public int G;
        public int H;
        public int F { get { return G + H; } }
        public AStarSquare Parent;
    }
    public static class AStar
    {
        public static List<Point> GetAPath(Point start, Point end, UInt16[] collision, int MapWidth, int maxLength = 0) { return GetAPath(start, end, collision.ToBitArray(), MapWidth, maxLength);  }
        public static List<Point> GetAPath(Point start, Point end, BitArray collision, int MapWidth, int maxLength = 0)
        {
            if (end == start)
                return new List<Point>();
            int MapHeight = collision.Length / MapWidth;
            List<AStarSquare> OpenNodes = new List<AStarSquare>();
            List<AStarSquare> ClosedNodes = new List<AStarSquare>();
            OpenNodes.Add(new AStarSquare() { P = start, G = 0, H = Math.Abs(start.X - end.X) + Math.Abs(start.Y - end.Y), Parent = null });
            AStarSquare endSquare = null;
            while (true)
            {
                int lowestF = int.MaxValue;
                int index = 0;
                for (int i = OpenNodes.Count - 1; i >= 0; i--)
                {
                    if (maxLength != 0 && OpenNodes[i].G > maxLength)
                    {
                        OpenNodes.RemoveAt(i);
                    }
                    else if (OpenNodes[i].F < lowestF)
                    {
                        lowestF = OpenNodes[i].F;
                        index = i;
                    }
                }

                if (OpenNodes.Count == 0)
                    break;

                AStarSquare currentNode = OpenNodes[index];

                OpenNodes.Remove(currentNode);
                ClosedNodes.Add(currentNode);

                if (currentNode.P == end)
                {
                    endSquare = currentNode;
                    break;
                }

                Point[] targetPoints = new Point[4];
                targetPoints[0] = new Point(currentNode.P.X, currentNode.P.Y - 1); //top
                targetPoints[1] = new Point(currentNode.P.X + 1, currentNode.P.Y); //right
                targetPoints[2] = new Point(currentNode.P.X, currentNode.P.Y + 1); //bottom
                targetPoints[3] = new Point(currentNode.P.X - 1, currentNode.P.Y); //left
                for (int i = 0; i < targetPoints.Length; i++)
                {
                    int tile = targetPoints[i].X + targetPoints[i].Y * MapWidth;
                    if (targetPoints[i].X < 0 || targetPoints[i].X >= MapWidth || targetPoints[i].Y < 0 || targetPoints[i].Y >= MapHeight || collision[tile])
                        continue;
                    if (ClosedNodes.Find(c => c.P == targetPoints[i]) == null) // not in closed list
                    {
                        int g = currentNode.G + 1;
                        int h = Math.Abs(targetPoints[i].X - end.X) + Math.Abs(targetPoints[i].Y - end.Y);
                        var openNode = OpenNodes.Find(c => c.P == targetPoints[i]);
                        if (openNode != null && g + h < openNode.F) // if already in open list check if this is better path
                        {
                            openNode.Parent = currentNode;
                            openNode.G = g;
                        }
                        else if (openNode == null) // not in open list
                            OpenNodes.Add(new AStarSquare() { Parent = currentNode, P = targetPoints[i], G = g, H = h });
                    }
                }
            }

            List<Point> returnList = new List<Point>();
            if (endSquare != null)
            {
                AStarSquare curNode = endSquare;
                while (curNode != null)
                {
                    returnList.Add(curNode.P);
                    curNode = curNode.Parent;
                }
            }
            return returnList;
        }
    }
}
