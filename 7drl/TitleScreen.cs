using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNA_GUI_Controls;


namespace RunRogue
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TitleScreen : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Controller guiControl;
        Texture2D background;
        Texture2D copyright;
        byte screen = 0;
        public TitleScreen(Game game)
            : base(game)
        {

        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            Main.graphics.DeviceReset += OnResize;
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            Main.graphics.DeviceReset -= OnResize;
            base.Dispose(disposing);
        }
        void OnResize(object sender, EventArgs args)
        {
            ShowMain();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            background = Game.Content.Load<Texture2D>("background");
            copyright = Game.Content.Load<Texture2D>(@"GUI/copyright");
            guiControl = Main.guiControl;

            ShowMain();

            base.LoadContent();
        }
        void ShowTitle()
        {
            int w2 = GraphicsDevice.Viewport.Width / 2;
            TextArea title = guiControl.AddTextArea("title", "Run Rogue", new Rectangle(w2 - 200, 24, 400, 64));
            title.Font = Main.fontFipps;
            title.Color = new Color(133, 160, 180);


            Sprite s = guiControl.AddSprite("copyright", new Rectangle(w2 - 100, GraphicsDevice.Viewport.Height - 16 - 32 - 8, 16, 16), copyright);
            TextArea copyr = guiControl.AddTextArea("copyr", "2013 Peter Gruden", new Rectangle(w2 - 100 + 24, GraphicsDevice.Viewport.Height - 80, 200, 64));
            copyr.Color = Color.White;
            copyr.Alignment = TextAlignment.Left;

            TextArea version = guiControl.AddTextArea("ver", "v 1.0", new Rectangle(GraphicsDevice.Viewport.Width - 80, GraphicsDevice.Viewport.Height - 40, 80, 32));
            version.Color = Color.LightGray;
        }
        void ShowMain()
        {
            guiControl.ClearChildren();
            ShowTitle();
            screen = 0;

            int w2 = GraphicsDevice.Viewport.Width / 2;
            int h2 = GraphicsDevice.Viewport.Height / 2;
            Button b = guiControl.AddButton("continue", "Continue", new Rectangle(w2 - 100, h2 - 124, 200, 50), OnButtonClick, 0);
            if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "savegame.rrs")))
                b.Disabled = true;
            guiControl.AddButton("newgame", "New Game", new Rectangle(w2 - 100, h2 - 58, 200, 50), OnButtonClick, 0);
            guiControl.AddButton("options", "Settings", new Rectangle(w2 - 100, h2 + 8, 200, 50), OnButtonClick, 0);
            guiControl.AddButton("credits", "Credits", new Rectangle(w2 - 100, h2 + 74, 200, 50), OnButtonClick, 0);
            guiControl.AddButton("exit", "Exit", new Rectangle(w2 - 100, h2 + 133, 200, 50), OnButtonClick, 0);
        }
        void ShowNewGame()
        {
            guiControl.ClearChildren();
            ShowTitle();

            screen = 1;
            int w2 = GraphicsDevice.Viewport.Width / 2;
            int h2 = GraphicsDevice.Viewport.Height / 2;

            Button b = guiControl.AddButton("startgame", "Easy", new Rectangle(w2 - 100, h2 - 25 - 16 - 50, 200, 50), OnButtonClick, 0);
            b.Tag = 0;
            b = guiControl.AddButton("startgame", "Normal", new Rectangle(w2 - 100, h2 - 25, 200, 50), OnButtonClick, 0);
            b.Tag = 1;
            b = guiControl.AddButton("startgame", "Hard", new Rectangle(w2 - 100, h2 + 25 + 16, 200, 50), OnButtonClick, 0);
            b.Tag = 2;

        }
        void ShowCredits()
        {
            guiControl.ClearChildren();
            ShowTitle();
            screen = 3;

            var frame = guiControl.AddFrame("credits", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 175, GraphicsDevice.Viewport.Height / 2 - 125, 350, 250), (int)GUIFrameStyle.Holy);
            Rectangle inner = frame.InnerRect;
            int w2 = (int)(inner.Width * 0.4f);
            var text = guiControl.AddTextArea("code", "Code & Design:", new Rectangle(inner.X, inner.Y, w2, 50), frame);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("code", "InanZen", new Rectangle(inner.X + w2 + 16, inner.Y, w2, 50), frame);
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkBlue;
            text.OnClick += OnCreditClick;

            text = guiControl.AddTextArea("music", "Music:", new Rectangle(inner.X, inner.Y + 50, w2, 50), frame);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("music", "Matthew Pablo", new Rectangle(inner.X + w2 + 16, inner.Y + 50, w2, 50), frame);
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkCyan;
            text.OnClick += OnCreditClick;

            text = guiControl.AddTextArea("art", "Art:", new Rectangle(inner.X, inner.Y + 100, w2, 50), frame);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("art", "OpenGameArt", new Rectangle(inner.X + w2 + 16, inner.Y + 100, w2, 50), frame);
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkCyan;
            text.OnClick += OnCreditClick;

            var button = guiControl.AddButton("main", "Back", new Rectangle(inner.Center.X - 100, inner.Bottom - 32, 200, 32), OnButtonClick, (int)GUIButtonStyle.Blue, frame);
        }
        void OnCreditClick(object sender, EventArgs args)
        {
            try
            {
                Control c = sender as Control;
                if (c.Name == "code")
                    System.Diagnostics.Process.Start("http://inanzen.eu/");
                else if (c.Name == "music")
                    System.Diagnostics.Process.Start("http://www.matthewpablo.com/");
                else if (c.Name == "art")
                    System.Diagnostics.Process.Start("http://opengameart.org/");
            }
            catch { }
        }

        void OnVolumeChange(object sender, EventArgs e)
        {
            Slider s = sender as Slider;
            switch (s.Name)
            {
                case "musicVolumeSlider":
                    {
                        MediaPlayer.Volume = s.Value;
                        TextArea volTxt = guiControl.FindChild("musicVolumeText") as TextArea;
                        volTxt.Text = String.Format("Music volume: {0}%", (int)(s.Value * 100));
                        break;
                    }
                case "sfxVolumeSlider":
                    {
                        SoundEffect.MasterVolume = s.Value;
                        TextArea volTxt = guiControl.FindChild("sfxVolumeText") as TextArea;
                        volTxt.Text = String.Format("SFX volume: {0}%", (int)(s.Value * 100));
                        break;
                    }

            }
        }

        void OnButtonClick(object sender, EventArgs args)
        {
            var button = sender as Button;
            switch (button.Name)
            {
                case "exit":
                    {
                        Game.Exit();
                        break;
                    }
                case "continue":
                    {
                        GameComponent game = new GameComponent(Game);
                        Game.Components.Add(game);
                        if (!game.LoadGame())
                            game.NewGame(1);
                        Game.Components.Remove(this);
                        this.Dispose();
                        break;
                    }
                case "credits":
                    {
                        ShowCredits();
                        break;
                    }
                case "main":
                    {
                        ShowMain();
                        break;
                    }
                case "newgame":
                    {
                        ShowNewGame();
                        break;
                    }
                case "startgame":
                    {
                        GameComponent game = new GameComponent(Game);
                        Game.Components.Add(game);
                        game.NewGame((int)button.Tag);
                        Game.Components.Remove(this);
                        this.Dispose();
                        break;
                    }
                case "options":
                    {
                        FrameBox options = guiControl.FindChild("optionsFrame") as FrameBox;
                        if (options == null)
                        {
                            options = guiControl.AddFrame("optionsFrame", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 200, GraphicsDevice.Viewport.Height / 2 - 200, 400,400), (int)GUIFrameStyle.Default, button.Parent);
                            int lineCount = 6;
                            int margin = 16;
                            int lineH = (options.Position.Height - 2 * margin) / lineCount;
                            int lineW = options.Position.Width - 2 * margin;
                            for (int line = 0; line < lineCount; line++)
                            {
                                int lineY = options.Position.Y + line * lineH + margin;
                                if (line == 0)
                                {
                                    float v = MediaPlayer.Volume;
                                    guiControl.AddTextArea("musicVolumeText", String.Format("Music volume: {0}%", (int)(v * 100)), new Rectangle(options.Position.X + margin, lineY, lineW / 2, lineH), options);
                                    var slider = guiControl.AddSlider("musicVolumeSlider", new Rectangle(options.Position.X + margin + lineW / 2, lineY + lineH / 2 - 12, lineW / 2, 24), (int)GUIBackgroundStyle.Slider, (int)GUIButtonStyle.Slider, options);
                                    slider.Value = v;
                                    slider.ValueChanged += OnVolumeChange;
                                }
                                else if (line == 1)
                                {
                                    float v = SoundEffect.MasterVolume;
                                    guiControl.AddTextArea("sfxVolumeText", String.Format("SFX volume: {0}%", (int)(v * 100)), new Rectangle(options.Position.X + margin, lineY, lineW / 2, lineH), options);
                                    var slider = guiControl.AddSlider("sfxVolumeSlider", new Rectangle(options.Position.X + margin + lineW / 2, lineY + lineH / 2 - 12, lineW / 2, 24), (int)GUIBackgroundStyle.Slider, (int)GUIButtonStyle.Slider, options);
                                    slider.Value = v;
                                    slider.ValueChanged += OnVolumeChange;
                                }
                                else if (line == 2)
                                {
                                    guiControl.AddTextArea("resolutionText", "Resolution:", new Rectangle(options.Position.X + margin, lineY, lineW / 2, lineH), options);
                                    Point res = new Point(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
                                    TextArea resolution = guiControl.AddTextArea("resolutionValue", String.Format("{0}x{1}",res.X, res.Y), new Rectangle(options.Position.X + lineW / 2 + margin, lineY, lineW / 2, lineH), options);
                                    resolution.Tag = res;
                                    var buttonL = guiControl.AddButton("resolutionChangeL", null, new Rectangle(options.Position.X + lineW / 2 + margin, lineY + lineH / 2 - 16, 32, 32), OnButtonClick, (int)GUIButtonStyle.Left, options);
                                    var buttonR = guiControl.AddButton("resolutionChangeR", null, new Rectangle(options.Position.Right - margin - 32, lineY + lineH / 2 - 16, 32, 32), OnButtonClick, (int)GUIButtonStyle.Right, options);
                                    buttonL.Tag = -1;
                                    buttonR.Tag = 1;
                                    if (Main.currentDispMode == 0)
                                        buttonL.Disabled = true;
                                    if (Main.currentDispMode == Main.displayModes.Length - 1)
                                        buttonR.Disabled = true;                                    
                                }
                                else if (line == 3)
                                {
                                    guiControl.AddTextArea("fullscreenText", "Fullscreen:", new Rectangle(options.Position.X + margin, lineY, lineW / 2, lineH), options);
                                    var fsbutton = guiControl.AddToggleButton("fsCheck", null, new Rectangle(options.Position.Center.X + margin, lineY, 64, 64), null, (int)GUIButtonStyle.CheckBox, options);
                                    if (Main.graphics.IsFullScreen)
                                        fsbutton.State = 1;
                                }
                                else if (line == lineCount - 1)
                                {
                                    guiControl.AddButton("saveSettings", "Apply", new Rectangle(options.Position.Center.X - 50, lineY, 100, lineH), OnButtonClick, (int)GUIButtonStyle.Green, options);                                    
                                }
                            }
                            screen = 2;
                        }
                        break;
                    }
                case "resolutionChangeR":
                case "resolutionChangeL":
                    {
                        Control options = button.Parent;
                        TextArea resVal = options.FindChild("resolutionValue") as TextArea;
                        Main.currentDispMode += (int)button.Tag;
                        DisplayMode dm = Main.displayModes[Main.currentDispMode];
                        resVal.Text = String.Format("{0}x{1}", dm.Width, dm.Height);
                        resVal.Tag = new Point(dm.Width, dm.Height);
                        Button lB = options.FindChild("resolutionChangeL") as Button;
                        Button rB = options.FindChild("resolutionChangeR") as Button;
                        if (Main.currentDispMode == 0)
                            lB.Disabled = true;
                        else
                            lB.Disabled = false;
                        if (Main.currentDispMode == Main.displayModes.Length - 1)
                            rB.Disabled = true;
                        else
                            rB.Disabled = false;
                        break;
                    }
                case "saveSettings":
                    {
                        try
                        {
                            FrameBox options = button.Parent as FrameBox;
                            Point resolution = (Point)options.FindChild("resolutionValue").Tag;
                            ToggleButton fullScr = options.FindChild("fsCheck") as ToggleButton;
                            bool full = false;
                            if (fullScr.State == 1)
                                full = true;
                            Main.graphics.PreferredBackBufferWidth = resolution.X;
                            Main.graphics.PreferredBackBufferHeight = resolution.Y;
                            Main.graphics.IsFullScreen = full;
                            Main.graphics.ApplyChanges();


                            using (FileStream stream = new FileStream(Path.Combine(Environment.CurrentDirectory, "settings.rrs"), FileMode.Create, FileAccess.Write, FileShare.None))
                            {
                                using (BinaryWriter writer = new BinaryWriter(stream))
                                {
                                    writer.Write(MediaPlayer.Volume);
                                    writer.Write(SoundEffect.MasterVolume);
                                    writer.Write(resolution.X);
                                    writer.Write(resolution.Y);
                                    writer.Write(full);
                                }
                            }
                            guiControl.Remove(options);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                        screen = 0;
                        break;
                    }
            }
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            HandleInput(gameTime);
            base.Update(gameTime);
        }
        void HandleInput(GameTime gameTime)
        {
            KeyboardState newKeyboardState = Keyboard.GetState();
            if (newKeyboardState.IsKeyDown(Keys.Escape))
            {
                if (screen == 0)
                    Game.Exit();
                else
                    ShowMain();
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
