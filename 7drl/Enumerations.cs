﻿namespace RunRogue
{
    enum SpecialTile
    {
        Stairs = 1,
        Treasure = 2,
        Chest = 3
    }
    
    enum NpcState
    {
        Idle = 0,
        Transition = 1,
        Done = 2,
        Waiting = 3
    }
    enum BodyPart
    {
        Cape = 0,
        Head = 1,
        Armor = 2,
        Arms = 3,
        Legs = 4,
        Feet = 5,
        Weapon = 6,
        Shield = 7,
        Body = 8
    }
    enum AttackType
    {
        Melee = 0,
        Ranged = 1,
        Magic = 2
    }
    enum GameState
    {
        TitleScreen = 0,
        Game = 1
    }


    enum GUIButtonStyle
    {
        Blue = 0,
        Green = 1,
        Up = 2,
        Right = 3,
        Down = 4,
        Left = 5,
        Close = 6,
        Slider = 7,
        CheckBox = 8,
        Inventory = 9,
        Map = 10,
        Menu = 11
    }
    enum GUIBackgroundStyle
    {
        Rock = 0,
        Slider = 1
    }
    enum GUIFrameStyle
    {
        Default = 0,
        Flowers = 1,
        Holy = 2
    }
}
