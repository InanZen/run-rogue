using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RunRogue
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AudioManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public List<Song> SongList = new List<Song>();
        List<SoundEffect> SoundQueue = new List<SoundEffect>();
        public bool Muted = false;
        SoundEffect[] HitSounds;
        SoundEffect[] MagicSounds;
        SoundEffect[] PotionSounds;
        SoundEffect[] DeathSounds;
        SoundEffect PickupSound;


        public AudioManager(Game game)
            : base(game)
        {
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }
        protected override void  Dispose(bool disposing)
        {
            try
            {
                MediaPlayer.Stop();
                foreach (Song s in SongList)
                    s.Dispose();
                SoundQueue.Clear();
                for (int i = 0; i < HitSounds.Length; i++)
                    HitSounds[i].Dispose();
                for (int i = 0; i < MagicSounds.Length; i++)
                    MagicSounds[i].Dispose();
                for (int i = 0; i < PotionSounds.Length; i++)
                    PotionSounds[i].Dispose();
                for (int i = 0; i < DeathSounds.Length; i++)
                    DeathSounds[i].Dispose();
                if (disposing)
                    GC.SuppressFinalize(this);
            }
            catch
            {
            }
            base.Dispose(disposing);
        }
        protected override void LoadContent()
        {
            SongList.Add(Game.Content.Load<Song>(@"Music/Grassy World"));
            SongList.Add(Game.Content.Load<Song>(@"Music/Dark Descent"));
            SongList.Add(Game.Content.Load<Song>(@"Music/Deliciously Sour"));
            SongList.Add(Game.Content.Load<Song>(@"Music/Evasion"));
            SongList.Add(Game.Content.Load<Song>(@"Music/Lurid Delusion"));
            SongList.Add(Game.Content.Load<Song>(@"Music/Soliloquy"));

            HitSounds = new SoundEffect[2];
            MagicSounds = new SoundEffect[2];
            PotionSounds = new SoundEffect[2];
            DeathSounds = new SoundEffect[1];
            HitSounds[0] = Game.Content.Load<SoundEffect>(@"Sounds/hit1");
            HitSounds[1] = Game.Content.Load<SoundEffect>(@"Sounds/hit2");
            MagicSounds[0] = Game.Content.Load<SoundEffect>(@"Sounds/magic1");
            MagicSounds[1] = Game.Content.Load<SoundEffect>(@"Sounds/magic2");
            PotionSounds[0] = Game.Content.Load<SoundEffect>(@"Sounds/bubble");
            PotionSounds[1] = Game.Content.Load<SoundEffect>(@"Sounds/bubble2");
            DeathSounds[0] = Game.Content.Load<SoundEffect>(@"Sounds/WilhelmScream");
            PickupSound = Game.Content.Load<SoundEffect>(@"Sounds/pickup");
            base.LoadContent();
        }
        public void HitSound()
        {
            int indx = Main.GetRandom(0, HitSounds.Length);
            SoundQueue.Add(HitSounds[indx]);
        }
        public void MagicSound()
        {
            int indx = Main.GetRandom(0, MagicSounds.Length);
            SoundQueue.Add(MagicSounds[indx]);
        }
        public void PotionSound()
        {
            int indx = Main.GetRandom(0, PotionSounds.Length);
            SoundQueue.Add(PotionSounds[indx]);
        }
        public void DeathSound()
        {
            int indx = Main.GetRandom(0, DeathSounds.Length);
            SoundQueue.Add(DeathSounds[indx]);
        }
        public void PickUpSound()
        {
            SoundQueue.Add(PickupSound);
        }

        public void SetVolume(float amt)
        {
            MediaPlayer.Volume = amt;
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (Game.IsActive)
            {
                if (MediaPlayer.State == MediaState.Paused)
                    MediaPlayer.Resume();
                switch (Main.gameState)
                {
                    case GameState.TitleScreen:
                        {
                            if (MediaPlayer.State != MediaState.Playing || MediaPlayer.Queue.ActiveSong.Name != SongList[0].Name)
                            {
                                MediaPlayer.Play(SongList[0]);                                
                            }
                            break;
                        }
                    case GameState.Game:
                        {
                            if (MediaPlayer.State != MediaState.Playing)
                            {
                                MediaPlayer.Play(SongList[Main.GetRandom(0, SongList.Count)]);
                            }
                            break;
                        }
                }
                for (int i = SoundQueue.Count - 1; i >= 0; i--)
                {
                    SoundQueue[i].Play();
                    SoundQueue.RemoveAt(i);
                }
            }
            else if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Pause();
            }
            base.Update(gameTime);
        }
    }
}
