using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNA_GUI_Controls;

namespace RunRogue
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        internal static GraphicsDeviceManager graphics;
        internal static GameState gameState;
        internal static DisplayMode[] displayModes;
        internal static int currentDispMode = 0;
        internal static Controller guiControl;
        internal static SpriteFont font;
        internal static SpriteFont fontFipps;

        private static readonly Object _randLocker = new object();
        protected static Random rand = new Random();

        SpriteBatch spriteBatch;
        BasicEffect ef;
        Texture2D cursor;
        MouseState mouseState;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.DeviceResetting += ongraphicsreset;
            Content.RootDirectory = "Content";
        }
        public void ongraphicsreset(object sender, EventArgs e)
        {          
            Vector2 diff = new Vector2((float)graphics.PreferredBackBufferWidth / graphics.GraphicsDevice.Viewport.Width, (float)graphics.PreferredBackBufferHeight / graphics.GraphicsDevice.Viewport.Height);
            if (guiControl != null)
                guiControl.Position = new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
        }
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsFixedTimeStep = true;

            displayModes = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes.ToArray();


            bool settingsLoaded = false;
            try
            {
                string settingsPath = Path.Combine(Environment.CurrentDirectory, "settings.rrs");
                if (File.Exists(settingsPath))
                {
                    using (FileStream stream = new FileStream(settingsPath, FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            MediaPlayer.Volume = reader.ReadSingle();
                            SoundEffect.MasterVolume = reader.ReadSingle();
                            graphics.PreferredBackBufferWidth = reader.ReadInt32();
                            graphics.PreferredBackBufferHeight = reader.ReadInt32();
                            graphics.IsFullScreen = reader.ReadBoolean();
                            for (int i = 0; i < displayModes.Length; i++)
                                if (displayModes[i].Width == graphics.PreferredBackBufferWidth && displayModes[i].Height == graphics.PreferredBackBufferHeight)
                                {
                                    currentDispMode = i;
                                    break;
                                }
                            settingsLoaded = true;
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            if (!settingsLoaded)
            {
                int diff = int.MaxValue;
                int preferedW = 1024;
                for (int i = 0; i < displayModes.Length; i++)
                    if (Math.Abs(displayModes[i].Width - preferedW) < diff)
                    {
                        diff = Math.Abs(displayModes[i].Width - preferedW);
                        currentDispMode = i;
                    }
                graphics.PreferredBackBufferWidth = displayModes[currentDispMode].Width;
                graphics.PreferredBackBufferHeight = displayModes[currentDispMode].Height;
            }
            graphics.ApplyChanges();

            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            System.Threading.Thread.Sleep(1000);
        }
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            ef = new BasicEffect(GraphicsDevice);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            cursor = Content.Load<Texture2D>("cursor");

            font = Content.Load<SpriteFont>("MainFont");
            fontFipps = Content.Load<SpriteFont>("fipps");

            guiControl = new Controller("controller", new Rectangle(0,0,GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), font);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/button_blue"), 287, 60);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/button_green"), 287, 60);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/up"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/right"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/down"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/left"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/closeButton1"), 15, 15);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/slider_button"), 14, 26);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/checkbox"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/inventory"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/map"), 32, 32);
            guiControl.AddButtonStyle(Content.Load<Texture2D>(@"GUI/menu"), 32, 32);

            guiControl.AddFrameStyle(Content.Load<Texture2D>(@"GUI/borders_18_bg"), 18, 18);
            guiControl.AddFrameStyle(Content.Load<Texture2D>(@"GUI/frame_8px_flowers"), 8, 8);
            guiControl.AddFrameStyle(Content.Load<Texture2D>(@"GUI/frame_11_holy"), 11, 11);

            guiControl.AddBackgroundStyle(Content.Load<Texture2D>(@"GUI/rock_bar"));
            guiControl.AddBackgroundStyle(Content.Load<Texture2D>(@"GUI/slider_bg"));


            AudioManager audio = new AudioManager(this);
            audio.Initialize();
            Components.Add(audio);
            TitleScreen title = new TitleScreen(this);
            title.Initialize();
            Components.Add(title);
            gameState = GameState.TitleScreen;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            mouseState = Mouse.GetState();
            guiControl.Update(gameTime);
            base.Update(gameTime);
        }
      

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(10, 20, 30));
            base.Draw(gameTime);
            ef.CurrentTechnique.Passes[0].Apply();
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            
                guiControl.Draw(spriteBatch);
                spriteBatch.Draw(cursor, new Vector2(mouseState.X, mouseState.Y), Color.White);
            
            spriteBatch.End();
        }
        internal static UInt16[] CombineLayers(params UInt16[][] layers)
        {
            UInt16[] combined = new UInt16[layers[0].Length];
            for (int i = 0; i < combined.Length; i++)
            {
                for (int l = 0; l < layers.Length; l++)                
                    combined[i] |= layers[l][i];                
            }
            return combined;
        }
        internal static UInt16[] InvertLayer(UInt16[] layer)
        {
            UInt16[] inverted = new UInt16[layer.Length];
            for (int i = 0; i < layer.Length; i++)
            {
                if (layer[i] == 0)
                    inverted[i] = 1;
                else
                    inverted[i] = 0;
            }
            return inverted;
        }

        internal static int GetRandom(int min, int max)
        {
            int r = min;
            lock (_randLocker)
            {
                try
                {
                    r = rand.Next(min, max);
                }
                catch { }
            }
            return r;
        }
    }
}
