﻿using Microsoft.Xna.Framework;

namespace RunRogue
{
    class Projectile
    {
        public Item OriginItem;
        public bool Active;
        public byte Direction;
        public Vector2 StartPos;
        public Vector2 EndPos;
        public float TransAmt;
        public Vector2 Position
        {
            get { return Vector2.Lerp(StartPos, EndPos, TransAmt); }
        }
        public int Owner;
        public Projectile(Vector2 start, Vector2 end, int owner, Item origin)
        {
            StartPos = start;
            EndPos = end;
            TransAmt = 0;
            Owner = owner;
            Active = true;
            OriginItem = origin;
            Vector2 diff = end - start;
            // double rot = Math.Atan2(diff.Y, diff.X);
            if (diff.X == 0)
            {
                Direction = 0;
                if (diff.Y > 0)
                    Direction = 1;
            }
            else
            {
                Direction = 2;
                if (diff.X > 0)
                    Direction = 3;
            }

        }
        public void Update(GameTime gameTime)
        {
            if (Active)
            {
                float dT = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                TransAmt += (1f / 500) * dT;
                if (TransAmt >= 1f)
                {
                    TransAmt = 0;
                    Active = false;
                }
            }
        }
    }
}
