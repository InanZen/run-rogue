﻿using Microsoft.Xna.Framework;
using System.ComponentModel;
using System;
using System.Collections;

namespace RunRogue
{
    public static class Extensions
    {
        public static Vector2 ToVector2(this Point p)
        {
            return new Vector2(p.X, p.Y);
        }
        public static Point ToPoint(this Vector2 v)
        {
            return new Point((int)v.X, (int)v.Y);
        }
        public static BitArray ToBitArray(this UInt16[] a)
        {
            int len = a.Length;
            BitArray ba = new BitArray(len);
            for (int i = 0; i < len; i++)
            {
                if (a[i] != 0)
                    ba[i] = true;
            }
            return ba;
        }
        public static BitArray ToBitArray(this Byte[] a)
        {
            int len = a.Length;
            BitArray ba = new BitArray(len);
            for (int i = 0; i < len; i++)
            {
                if (a[i] != 0)
                    ba[i] = true;
            }
            return ba;
        }
 
    }
    public class ObjectEventArgs : HandledEventArgs
    {
        public Object Tag;
        public ObjectEventArgs(Object tag)
        {
            Tag = tag;
        }
    }
    public struct IntInt
    {
        public int A;
        public int B;
        public IntInt(int a, int b)
        {
            A = a;
            B = b;
        }
    }
}
