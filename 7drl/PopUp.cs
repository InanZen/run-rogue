﻿using System;
using XNA_GUI_Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RunRogue
{
    public class PopUp
    {
        public Control Host;
        public FrameBox Frame;
        public bool KeepMouseDistance;
        public Point MouseDistance = new Point(32, 32);
        public TextArea textArea;
        public string Text
        {
            get { return textArea.Text; }
            set { textArea.Text = value; }
        }
        public Rectangle Position
        {
            get { return Frame.Position; }
            set
            {
                Frame.Position = value;
            }
        }
        public void CloseEvent(object sender, EventArgs args)
        {
            try
            {
                if (Frame != null)
                {
                    Frame.Parent = null;
                    Frame.ClearChildren();
                }
                if (textArea != null)
                    textArea.Parent = null;
                Host.OnMouseLeave -= CloseEvent;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        }
        public void Update()
        {
            if (KeepMouseDistance)
            {
                MouseState ms = Mouse.GetState();
                Position = new Rectangle(ms.X + MouseDistance.X, ms.Y + MouseDistance.Y, Position.Width, Position.Height);
            }
        }
        public PopUp(Control host, Rectangle pos, int frameStyle, string text)
        {
            Host = host;
            Controller c = host.TopParent as Controller;
            Frame = c.AddFrame("popup_frame", pos, frameStyle);
            textArea = c.AddTextArea("popup_text", text, Frame.InnerRect, Frame);
            host.OnMouseLeave += CloseEvent;
        }
    }
    class NPCPupUp : PopUp
    {
        NPC Npc;
        int npchp;
        public void DisposeHost(object sender, EventArgs args)
        {
            try
            {
                Host.OnMouseLeave -= DisposeHost;
                Host.Parent = null;
                Host = null;
                Npc = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        }
        public new void Update()
        {
            if (Npc == null || Npc.HP <= 0)
            {
                if (Host != null)
                {
                    base.CloseEvent(this, EventArgs.Empty);
                    DisposeHost(this, EventArgs.Empty);
                }
            }
            else if (npchp != Npc.HP)
            {
                var hptxt = Frame.FindChild("hpval", false) as TextArea;
                if (hptxt != null)
                    hptxt.Text = Npc.HP.ToString();
                npchp = Npc.HP;
            }
            base.Update();
        }
        public NPCPupUp(NPC npc, Controller controller, Rectangle hostpos, Rectangle pos, int frameStyle, string text): base (new Control("pophost", hostpos) { Parent = controller },  pos, frameStyle, text)
        {
            Npc = npc;
            npchp = npc.HP;
            Host.OnMouseLeave += DisposeHost;
        } 
    }

}
