﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace RunRogue
{
    public class TileCollection
    {
        public Texture2D texture;
        public ushort tileW;
        public ushort tileH;
        /// <summary>
        /// Number of empty tiles at the end of the sheet
        /// </summary>
        public int PaddingEnd;
        /// <summary>
        /// Number of empty tiles at the start of the sheet
        /// </summary>
        public int Padding; 

        public int wTileCount { get { return texture.Width / tileW; } }
        public int hTileCount { get { return texture.Height / tileH; } }
        public int elementCount { get { return wTileCount * hTileCount - Padding - PaddingEnd; } }
        public UInt16 firstGID;

        public TileCollection(Texture2D Texture, ushort TileW, ushort TileH, UInt16 FirstGID)
        {
            this.texture = Texture;
            this.texture.Name = Texture.Name;
            this.tileW = TileW;
            this.tileH = TileH;
            this.firstGID = FirstGID;
        }
        public Rectangle GetSourceRect(UInt16 gID)
        {
            UInt16 setIndx = (UInt16)(gID - firstGID + Padding);
            return new Rectangle((setIndx % wTileCount) * tileW, (setIndx / wTileCount) * tileH, tileW, tileH);
        }
    } 
}
