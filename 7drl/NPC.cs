﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RunRogue
{
    class NPC
    {
        public ushort Index;
        public byte Type;
        public float TransAmt = 0;
        Point ot;
        public Point OldTile { get { return ot; } }
        public Vector2 OldPosition { get { return new Vector2((OldTile.X - game.startX) * game.tileW, (OldTile.Y - game.startY) * game.tileH); } }
        Point t;
        public Point Tile {
            get { return t; }
            set {
                ot = t;
                t = value;
                State = NpcState.Transition;
                TransAmt = 0;
            }
        }
        public Vector2 Position {
            get
            {
                Vector2 fpos = new Vector2((Tile.X - game.startX) * game.tileW, (Tile.Y - game.startY) * game.tileH);
                if (State == NpcState.Transition)
                {
                    Vector2 spos = OldPosition;
                    return Vector2.Lerp(spos, fpos, TransAmt);
                }
                return fpos;
            }
        }
        public event EventHandler StateChanged;
        NpcState state;
        public NpcState State
        {
            get { return state; }
            set
            {
                ObjectEventArgs args = new ObjectEventArgs(state);
                state = value;
                if (StateChanged != null)
                    StateChanged(this, args);
            }
        }
        public byte HP;
        public byte MaxAP;

        public byte APs;

        public Item[] Equipped = new Item[8];

        protected GameComponent game;

        public bool SeenPlayer = false;

        public NPC(GameComponent gc, ushort index, Point tile)
        {
            game = gc;
            Index = index;
            State = NpcState.Idle;
            t = tile;
            ot = tile;
        }
        public virtual void Update(GameTime gameTime)
        {
            if (State == NpcState.Transition)
            {
                float dT = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                TransAmt += (1f / 500) * dT;
                if (TransAmt >= 1f)
                {
                    State = NpcState.Done;
                    TransAmt = 0;
                }
            }
            else if (state == NpcState.Done)
                State = NpcState.Idle;
        }
    }
    class Player : NPC
    {
        public String Name;
        public Item[] Inventory = new Item[12];
        public Player(GameComponent gc, string name) : base(gc, 0, Point.Zero)
        {
            Name = name;
            MaxAP = 4;
            APs = 4;
        }
        public override void Update(GameTime gametime)
        {
            base.Update(gametime);
        }
        public bool PutItemInInventory(Item item)
        {
            for (int i = 0; i < Inventory.Length; i++)
                if (Inventory[i] == null)
                {
                    Inventory[i] = item;
                    return true;
                }
            return false;
        }
    }
}
