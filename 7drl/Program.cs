using System;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace RunRogue
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {

            AppDomain currentDomain = AppDomain.CurrentDomain;
#if !DEBUG
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(ApplicationThreadException);
#endif
            using (Main game = new Main())
            {
                game.Run();
            }
        }
        static void ApplicationThreadException(object sender, UnhandledExceptionEventArgs args)
        {
            var fromAddress = new System.Net.Mail.MailAddress("inansmtp@gmail.com", "Error Reporter");
            var toAddress = new System.Net.Mail.MailAddress("peter@guruden.si", "Peter");
            const string fromPassword = "smtp1243";
            const string subject = "RunRogue Crash report";
            Exception exception = (Exception)args.ExceptionObject;
            string body = exception.Message + "\n" + exception.Data + "\n" + exception.StackTrace + "\n" + exception.Source;

            var smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress) { Subject = subject, Body = body })
            {
                smtp.Send(message);
            }
            using (FileStream stream = new FileStream(Path.Combine(Environment.CurrentDirectory, "crash.txt"), FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(exception.ToString());
                }
            }
        }
    }
#endif
}

