using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using XNA_GUI_Controls;

namespace RunRogue
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameComponent : Microsoft.Xna.Framework.DrawableGameComponent
    {
        KeyboardState keyboardState;
        KeyboardState oldKeyboardState;
        MouseState mouseState;
        MouseState oldMouseState;
        private object _levelLocker = new object();
        Level level;
        public int tileW = 24;
        public int tileH = 24;
        public int startX = 0;
        public int startY = 0;
        int DungeonDepth = 0;
        int MaxDepth = 3;
        /// <summary>
        /// 0 - easy, 1 - normal, 2 - hard
        /// </summary>
        int Difficulty = 1;
        int Turn = 1;

        NPC[] NpcList = new NPC[0];
        NPC[] NpcUpdateList = new NPC[0]; 
        Player player;
        Projectile ActiveProjectile;
        TileCollection ArrowTileset;
        TileCollection SwordTileset;
        Texture2D Fireball;


        TileCollection[] CharSheets = new TileCollection[Enum.GetValues(typeof(BodyPart)).Length];
        TileCollection WallSet;
        TileCollection BossSheet;
        TileCollection Potions;
        TileCollection SpecialSheet;

        AudioManager AudioMan;
        SpriteBatch spriteBatch;
        Controller guiControl;
        Texture2D cursor;
        Thread generateThread;

        TileCollection markers;
        UInt16[] markerPos;
        UInt16[] npcPos;
        byte[] potionPos;
        Item[] DroppedItems = new Item[64];
        byte[] groundItems;
        byte[] specialTiles;


        BitArray visible;
        BitArray uncovered;

        Control grabbedControl;
        Texture2D miniMap;
        FrameBox StatusBar;        
        TextArea HpText;
        Control PauseMenu;
        TransText DmgText;

        PopUp popup;
        NPCPupUp npcinfo;

        static readonly ushort MeleeFirstGID = 1;
        static readonly ushort MeleeCount = 119;
        static readonly ushort RangedFirstGID = 129;
        static readonly ushort RangedCount = 14;
        static readonly ushort MagicFirstGID = 145;
        static readonly ushort MagicCount = 46;
        static readonly ushort ShieldFirstGID = 193;
        static readonly ushort ShieldCount = 36;
        static readonly string[] Adjectives = new string[] { "Mighty", "Big", "Powerful", "Graceful", "Sneaky", "Rusty", "Ancient", "Weak" };
        static readonly string[] ShieldString = new string[] { "Shield", "Buckler", "Guard" };
        static readonly string[] SwordString = new string[] { "Sword", "Cutlass", "Knife", "Dagger", "Stick", "Blade" };
        static readonly string[] BowString = new string[] { "Bow", "Crossbow" };



        public GameComponent(Game game)
            : base(game)
        {
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            AudioMan = Game.Components.First(c => c.GetType() == typeof(AudioManager)) as AudioManager;
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    if (generateThread != null && generateThread.IsAlive)
                        generateThread.Abort();
                }
                catch { }
            }
            base.Dispose(disposing);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            markers = new TileCollection(Game.Content.Load<Texture2D>("markers"), 32,32,1);
            cursor = Game.Content.Load<Texture2D>("cursor");
            mouseState = Mouse.GetState();

            CharSheets[(int)BodyPart.Cape] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Capes"), 32, 32, 1);
            CharSheets[(int)BodyPart.Cape].PaddingEnd = 5; // capes

            CharSheets[(int)BodyPart.Head] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Head"), 32, 32, 1);
            CharSheets[(int)BodyPart.Head].PaddingEnd = 10; // head

            CharSheets[(int)BodyPart.Armor] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Armor"), 32, 32, 1);
            CharSheets[(int)BodyPart.Armor].PaddingEnd = 5; // armor

            CharSheets[(int)BodyPart.Arms] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Arms"), 32, 32, 1);

            CharSheets[(int)BodyPart.Legs] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Legs"), 32, 32, 1);
            CharSheets[(int)BodyPart.Legs].PaddingEnd = 2; // legs

            CharSheets[(int)BodyPart.Feet] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Feet"), 32, 32, 1);

            CharSheets[(int)BodyPart.Weapon] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Weapons"), 32, 32, 1);
            CharSheets[(int)BodyPart.Weapon].PaddingEnd = 66;

            CharSheets[(int)BodyPart.Shield] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Weapons"), 32, 32, 1);
            CharSheets[(int)BodyPart.Shield].Padding = 192;
            CharSheets[(int)BodyPart.Shield].PaddingEnd = 28;

            CharSheets[(int)BodyPart.Body] = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Body"), 32, 32, 1);
            CharSheets[(int)BodyPart.Body].PaddingEnd = 19; // body


            BossSheet = new TileCollection(Game.Content.Load<Texture2D>(@"CharSheets/ProjectUtumno_Dragons"), 32, 32, 1);
            ArrowTileset = new TileCollection(Game.Content.Load<Texture2D>("arrow"), 32, 32, 0);
            SwordTileset = new TileCollection(Game.Content.Load<Texture2D>("sword"), 32, 32, 0);
            Potions = new TileCollection(Game.Content.Load<Texture2D>("potions"), 32, 32, 1);

            Fireball = Game.Content.Load<Texture2D>("fireball");
            WallSet = new TileCollection(Game.Content.Load<Texture2D>("walls"), 16, 16, 1);

            SpecialSheet = new TileCollection(Game.Content.Load<Texture2D>("specials"), 32, 32, 1);

            oldKeyboardState = Keyboard.GetState();

            guiControl = Main.guiControl;
            guiControl.ClearChildren();

            StatusBar = guiControl.AddFrame("statusbar", new Rectangle(0, 0, GraphicsDevice.Viewport.Width, 64), (int)GUIFrameStyle.Holy);
            
            guiControl.AddSprite("hpicon", new Rectangle(64, 16, 32, 32), Game.Content.Load<Texture2D>(@"GUI/heart_64"), StatusBar);
            HpText = guiControl.AddTextArea("hptext", "100", new Rectangle(96, 0, 64, 64), StatusBar);

            guiControl.AddButton("invicon", null, new Rectangle(GraphicsDevice.Viewport.Width - 192 + 16, 16, 32, 32), OnButtonClick, (int)GUIButtonStyle.Inventory, StatusBar);
            guiControl.AddButton("mapicon", null, new Rectangle(GraphicsDevice.Viewport.Width - 128 + 16, 16, 32, 32), OnButtonClick, (int)GUIButtonStyle.Map, StatusBar);
            guiControl.AddButton("menuicon", null, new Rectangle(GraphicsDevice.Viewport.Width - 64 + 16, 16, 32, 32), OnButtonClick, (int)GUIButtonStyle.Menu, StatusBar);

            int curX = 192;
            var text = guiControl.AddTextArea("apstext", "APs:", new Rectangle(curX, 8, 64, 24), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("aps", "2", new Rectangle(curX + 72, 8, 64, 24), StatusBar);
            text.Alignment = TextAlignment.Left;

            text = guiControl.AddTextArea("turntext", "Turn:", new Rectangle(curX, 32, 64, 24), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("turn", Turn.ToString(), new Rectangle(curX + 72, 32, 64, 24), StatusBar);
            text.Alignment = TextAlignment.Left;
            curX += 160;

            text = guiControl.AddTextArea("depthtxt", "Depth:", new Rectangle(curX, 8, 72, 24), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("depth", String.Format("{0} / {1}",DungeonDepth, MaxDepth), new Rectangle(curX + 80, 8, 80, 24), StatusBar);
            text.Alignment = TextAlignment.Left;

            text = guiControl.AddTextArea("npctext", "Enemies:", new Rectangle(curX, 32, 72, 24), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = guiControl.AddTextArea("npc", NpcList.Length.ToString(), new Rectangle(curX + 80, 32, 64, 24), StatusBar);
            text.Alignment = TextAlignment.Left;
            curX += 160;

            tileW = GraphicsDevice.Viewport.Width / 35;
            if (tileW < 24)
                tileW = 24;
            else if (tileW > 48)
                tileW = 48;
            tileH = tileW;
            base.LoadContent();
        }

        Point GetClosestEmptyWithPadding(int x, int y, int range, BitArray tiles, int w)
        {
            int cr = 0;
            int h = tiles.Length / w;
            while (cr <= range)
            {
                int startY = y - cr;
                int endY = y + cr;
                int startX = x - cr;
                int endX = x + cr;
                for (int y2 = startY; y2 <= endY; y2++)
                {
                    if (y2 < 1 || y2 >= h-1)
                        continue;
                    for (int x2 = startX; x2 <= endX; x2++)
                    {
                        if (x2 < 1 || x2 >= w-1)
                            continue;
                        int tile = x2 + y2 * w;
                        if (!tiles[tile] && !tiles[tile - 1] && !tiles[tile + 1] && !tiles[tile - w] && !tiles[tile + w] && !tiles[tile - w - 1] && !tiles[tile - w + 1] && !tiles[tile + w - 1] && !tiles[tile + w + 1])
                           return new Point(x2, y2);                        
                    }
                }
                cr++;
            }
            return Point.Zero;
        }
        Point GetClosestEmptyTile(int x, int y, int range, BitArray tiles, int w)
        {
            int cr = 0;
            int h = tiles.Length / w;
            while (cr <= range)
            {
                int startY = y - cr;
                int endY = y + cr;
                int startX = x - cr;
                int endX = x + cr;
                for (int y2 = startY; y2 <= endY; y2++)
                {
                    if (y2 < 0 || y2 >= h)
                        continue;
                    for (int x2 = startX; x2 <= endX; x2++)
                    {
                        if (x2 < 0 || x2 >= w)
                            continue;
                        int tile = x2 + y2 * w;
                        //if (!tiles[tile] && (y2 == startY || y2 == endY || x2 == startX || x2 == endX))
                        if (!tiles[tile])                        
                            return new Point(x2, y2);                        
                    }
                }
                cr++;
            }
            return Point.Zero;
        }
        void CenterCameraOnTile(int x, int y)
        {
            int tileCountY = GraphicsDevice.Viewport.Height / tileH;
            int tileCountX = GraphicsDevice.Viewport.Width / tileW;
            startX = x - tileCountX / 2;
            startY = y - tileCountY / 2;
        }
        public void NewGame(int difficulty)
        {
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, "savegame.rrs")))
            {
                try
                {
                    File.Delete(Path.Combine(Environment.CurrentDirectory, "savegame.rrs"));
                }
                catch { }
            }

            Difficulty = difficulty;
            if (difficulty == 0)
                MaxDepth = 5;
            else if (difficulty == 1)
                MaxDepth = 7;
            else
                MaxDepth = 9;

            player = new Player(this, "Player");
            player.Type = 8;
            player.Equipped[(int)BodyPart.Weapon] = GenerateItem(BodyPart.Weapon, 0);
            int bp = Main.GetRandom(1, 6);
            player.Equipped[bp] = GenerateItem((BodyPart)bp, 0);
            player.StateChanged += OnPlayerState;
            player.HP = 100;
            player.APs = 2;

            ShowStartDialog(0);
        }
        void ShowStartDialog(int page)
        {
            FrameBox frame = guiControl.FindChild("newgamedialog", false) as FrameBox;
            if (frame == null)
            {
                frame = guiControl.AddFrame("newgamedialog", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 250, GraphicsDevice.Viewport.Height / 2 - 200, 500, 400), 0);
                frame.BorderH = 10;
                frame.BorderW = 10;
            }
            frame.ClearChildren();
            Rectangle inner = frame.InnerRect;
            int w2 = inner.Width / 2;
            int padding = 16;
            int h = 24;
            int curY = inner.Y + padding;
            if (page == 0)
            {
                var text = guiControl.AddTextArea("txt1", "You are a Rouge -->", new Rectangle(inner.X, curY, w2, 64), frame);
                text.Alignment = TextAlignment.Right;
                var playerSheet = CharSheets[(byte)BodyPart.Body];
                var rSprite = guiControl.AddSprite("playerSprite", new Rectangle(inner.Center.X + padding, curY, 64, 64), playerSheet.texture, frame);
                rSprite.Source = playerSheet.GetSourceRect(player.Type);
                curY += 64 + padding;

                guiControl.AddTextArea("txt2",
                    String.Format("Your quest is to find the stash of gold somewhere on level {0} of the dungeon. Be warned, the deeper you go the tougher enemies you'll encounter. Don't be afraid to loot their corpses in search of a better gear.", MaxDepth),
                    new Rectangle(inner.X, curY, inner.Width, 128), frame);

                curY += 128 + padding;

                text = guiControl.AddTextArea("txt", "Good luck!", new Rectangle(inner.X, curY, inner.Width, 50), frame);
                text.Color = Color.DarkGreen;


                var button = guiControl.AddButton("infopage", "Tutorial", new Rectangle(inner.Center.X - padding - 128, inner.Bottom - 40, 128, 32), OnButtonClick, (int)GUIButtonStyle.Blue, frame);
                button.Tag = 1;
                guiControl.AddButton("start", "Start", new Rectangle(inner.Center.X + padding, inner.Bottom - 40, 128, 32), OnButtonClick, (int)GUIButtonStyle.Green, frame);
            }
            else if (page == 1) // movement page
            {
                var text = guiControl.AddTextArea("infotxt", "Movement", new Rectangle(inner.X, curY, inner.Width, h), frame);
                text.Color = Color.DarkBlue;
                curY += h + padding + 32;

                guiControl.AddTextArea("infotxt", "You have 2 action points (AP) per turn.", new Rectangle(inner.X, curY, inner.Width, h), frame);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "1 AP Move marker:", new Rectangle(inner.X, curY, w2 + 64, h), frame);
                text.Alignment = TextAlignment.Right;
                var sprite = guiControl.AddSprite("infoico", new Rectangle(inner.Center.X + padding + 64, curY, h, h), markers.texture, frame);
                sprite.Source = markers.GetSourceRect(1);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "2 AP Move marker:", new Rectangle(inner.X, curY, w2 + 64, h), frame);
                text.Alignment = TextAlignment.Right;
                sprite = guiControl.AddSprite("infoico", new Rectangle(inner.Center.X + padding + 64, curY, h, h), markers.texture, frame);
                sprite.Source = markers.GetSourceRect(7);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "1 AP Attack marker:", new Rectangle(inner.X, curY, w2 + 64, h), frame);
                text.Alignment = TextAlignment.Right;
                sprite = guiControl.AddSprite("infoico", new Rectangle(inner.Center.X + padding + 64, curY, h, h), markers.texture, frame);
                sprite.Source = markers.GetSourceRect(5);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "Stairs going deeper into the dungeon:", new Rectangle(inner.X, curY, w2 + 64, h), frame);
                text.Alignment = TextAlignment.Right;
                sprite = guiControl.AddSprite("infoico", new Rectangle(inner.Center.X + padding + 64, curY, h, h), SpecialSheet.texture, frame);
                sprite.Source = SpecialSheet.GetSourceRect(1);

                var button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X - 32 - padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Left, frame);
                button.Tag = page - 1;
                button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X + padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Right, frame);
                button.Tag = page + 1;
            }
            else if (page == 2) // potions
            {
                var text = guiControl.AddTextArea("infotxt", "Potions", new Rectangle(inner.X, curY, inner.Width, h), frame);
                text.Color = Color.DarkViolet;
                curY += h + padding + 64;

                text = guiControl.AddTextArea("infotxt", "Potions restore your health (max: 100) and give boost to your APs", new Rectangle(inner.X + 8, curY, inner.Width - 16, h * 2), frame);
                text.Alignment = TextAlignment.Center;
                curY += h * 2 + padding;

                text = guiControl.AddTextArea("infotxt", "+10 HP health potion:", new Rectangle(inner.X, curY, w2 + 64, h), frame);
                text.Alignment = TextAlignment.Right;
                var sprite = guiControl.AddSprite("infoico", new Rectangle(inner.Center.X + padding + 64, curY, h, h), Potions.texture, frame);
                sprite.Source = Potions.GetSourceRect(1);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "+3 AP stamina potion:", new Rectangle(inner.X, curY, w2 + 64, h), frame);
                text.Alignment = TextAlignment.Right;
                sprite = guiControl.AddSprite("infoico", new Rectangle(inner.Center.X + padding + 64, curY, h, h), Potions.texture, frame);
                sprite.Source = Potions.GetSourceRect(2);
                curY += h + padding;

                var button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X - 32 - padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Left, frame);
                button.Tag = page - 1;
                button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X + padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Right, frame);
                button.Tag = page + 1;
            }
            else if (page == 3) // enemies page
            {
                var text = guiControl.AddTextArea("infotxt", "Enemies", new Rectangle(inner.X, curY, inner.Width, h), frame);
                text.Color = Color.DarkRed;
                curY += h + padding;


                text = guiControl.AddTextArea("infotxt", "After each of your turns the enemies get their turn. Each has 2 APs to spend on either movement, attack or both.", new Rectangle(inner.X + 8, curY, inner.Width - 16, h * 3), frame);
                text.Alignment = TextAlignment.Center;
                curY += h * 3 + padding;

                text = guiControl.AddTextArea("infotxt", "Right-click on an enemy to see their stats", new Rectangle(inner.X + 8, curY, inner.Width - 16, h), frame);
                text.Alignment = TextAlignment.Center;
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "When you kill an enemy, they will drop a piece of their euqipment on the tile they were standing on.", new Rectangle(inner.X + 8, curY, inner.Width - 16, h * 3), frame);
                text.Alignment = TextAlignment.Center;

                var button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X - 32 - padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Left, frame);
                button.Tag = page - 1;
                button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X + padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Right, frame);
                button.Tag = page + 1;
            }
            else if (page == 4) // items
            {
                var text = guiControl.AddTextArea("infotxt", "Items", new Rectangle(inner.X, curY, inner.Width, h), frame);
                text.Color = Color.DarkGreen;
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "You can equip and manage items from your Inventory", new Rectangle(inner.X, curY, inner.Width, h), frame);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "Attack types", new Rectangle(inner.X + 8, curY, inner.Width - 16, h), frame);
                curY += h;
                text = guiControl.AddTextArea("infotxt", "Melee: short range, all directions", new Rectangle(inner.X, curY, inner.Width, h), frame);
                curY += h + padding / 2;
                text = guiControl.AddTextArea("infotxt", "Ranged: can only hit in straight line", new Rectangle(inner.X, curY, inner.Width, h), frame);
                curY += h + padding / 2;
                text = guiControl.AddTextArea("infotxt", "Magic: can only hit in diagonal line", new Rectangle(inner.X, curY, inner.Width, h), frame);
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "A piece of armor can shield one or more damage types. When hit you receive less demage for each point of armor rating of the appropriate attack type.", new Rectangle(inner.X, curY, inner.Width, h * 4), frame);


                var button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X - 32 - padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Left, frame);
                button.Tag = page - 1;
                button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X + padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Right, frame);
                button.Tag = page + 1;
            }
            else // keybindings
            {
                var text = guiControl.AddTextArea("infotxt", "Keybindings", new Rectangle(inner.X, curY, inner.Width, h), frame);
                text.Color = Color.DarkSlateGray;
                curY += h + padding + 32;

                text = guiControl.AddTextArea("infotxt", "Inventory:", new Rectangle(inner.X, curY, w2, h), frame);
                text.Alignment = TextAlignment.Right;
                text = guiControl.AddTextArea("infotxt", "i", new Rectangle(inner.Center.X + padding, curY, w2, h), frame);
                text.Alignment = TextAlignment.Left;
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "Minimap:", new Rectangle(inner.X, curY, w2, h), frame);
                text.Alignment = TextAlignment.Right;
                text = guiControl.AddTextArea("infotxt", "m", new Rectangle(inner.Center.X + padding, curY, w2, h), frame);
                text.Alignment = TextAlignment.Left;
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "Pause:", new Rectangle(inner.X, curY, w2, h), frame);
                text.Alignment = TextAlignment.Right;
                text = guiControl.AddTextArea("infotxt", "Esc", new Rectangle(inner.Center.X + padding, curY, w2, h), frame);
                text.Alignment = TextAlignment.Left;
                curY += h + padding;

                text = guiControl.AddTextArea("infotxt", "Zoom:", new Rectangle(inner.X, curY, w2, h), frame);
                text.Alignment = TextAlignment.Right;
                text = guiControl.AddTextArea("infotxt", "+ -", new Rectangle(inner.Center.X + padding, curY, w2, h), frame);
                text.Alignment = TextAlignment.Left;
                curY += h + padding;

                var button = guiControl.AddButton("infopage", null, new Rectangle(inner.Center.X - 32 - padding, inner.Bottom - 40, 32, 32), OnButtonClick, (int)GUIButtonStyle.Left, frame);
                button.Tag = page - 1;
                guiControl.AddButton("start", "Start", new Rectangle(inner.Center.X + padding, inner.Bottom - 40, 128, 32), OnButtonClick, (int)GUIButtonStyle.Green, frame);

            }
        }

        #region Level Generator
        void StartLvlGen()
        {
            if (generateThread == null || !generateThread.IsAlive)
            {
                generateThread = new Thread(new ThreadStart(GenerateLevel));
                generateThread.Start();
            }
        }

        void GenerateLevel()
        {
            DungeonDepth++;  
            float depthMod = (float)DungeonDepth / MaxDepth;
            if (depthMod > 1f)
                depthMod = 1f;

            int minW = (int)(MathHelper.Lerp(32, 64, depthMod));
            int w = Main.GetRandom(minW, minW + 12);
            int h = Main.GetRandom(minW, minW + 12);

            var noticeBox = guiControl.AddFrame("gennotice", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 150, GraphicsDevice.Viewport.Height / 2 - 50, 300, 100), 0);
            guiControl.AddTextArea("gennotice_text", String.Format("Generating dungeon layer {0}...", DungeonDepth), noticeBox.Position, noticeBox);

            var layers = GenerateLayers(w, h);
            lock (_levelLocker)
            {
                level = new Level() { width = (short)w, height = (short)h, layers = layers, tileSets = new TileCollection[] { WallSet } };
            }


            npcPos = new UInt16[w * h];
            visible = new BitArray(w * h);
            uncovered = new BitArray(w * h);
            potionPos = new byte[w * h];
            groundItems = new byte[w * h];
            specialTiles = new byte[w * h];
            NpcUpdateList = new NPC[0];


            BitArray validTiles = level.layers.First(l => l.type == LayerType.Floor).gIDs.ToBitArray();
            BitArray invalidTiles = validTiles.Not();

            int npcmin = 3;
            int npcmax = 10;
            int npcminHP = 15;
            int npcmaxHP = 35;
            if (Difficulty == 1)
            {
                npcmin = 5;
                npcmax = 15;
                npcminHP = 20;
                npcmaxHP = 50;
            }
            else if (Difficulty == 2)
            {
                npcmin = 7;
                npcmax = 20;
                npcminHP = 20;
                npcmaxHP = 70;
            }
            int npccount = (int)(MathHelper.Lerp(npcmin, npcmax, depthMod));
            ushort index = 0;
            NpcList = new NPC[npccount];
            while (index < npccount)
            {
                int x = Main.GetRandom(1, level.width);
                int y = Main.GetRandom(1, level.height);
                Point tile = GetClosestEmptyTile(x, y, 4, invalidTiles, level.width);
                if (tile != Point.Zero)
                {
                    var npc = new NPC(this, index, tile);
                    npc.State = NpcState.Idle;
                    npc.HP = (byte)(MathHelper.Lerp(npcminHP, npcmaxHP, depthMod) + Main.GetRandom(0, npcmin / 2) - npcmin / 4);
                    npc.Type = (byte)Main.GetRandom(6, 38);
                    npc.APs = 2;

                    int maxItems = Main.GetRandom((int)MathHelper.Lerp(1, 3 + Difficulty, depthMod), (int)MathHelper.Lerp(4, 6 + Difficulty, depthMod));
                    for (int i = 0; i < npc.Equipped.Length; i++)
                    {
                        switch ((BodyPart)i)
                        {
                            case BodyPart.Weapon:
                                {                                    
                                    npc.Equipped[i] = GenerateItem((BodyPart)i, depthMod);
                                    break;
                                }
                            case BodyPart.Shield:
                                {
                                    if (maxItems > 0)
                                    {
                                        npc.Equipped[i] = GenerateItem((BodyPart)i, depthMod);
                                        maxItems--;
                                    }
                                    break;
                                }
                            case BodyPart.Cape:
                                {
                                    if (Main.GetRandom(0, 100) < 8)
                                        npc.Equipped[i] = GenerateItem((BodyPart)i, depthMod);
                                    break;
                                }
                            default:
                                {
                                    if (maxItems > 0 && Main.GetRandom(0, 100) < 50)
                                    {
                                        npc.Equipped[i] = GenerateItem((BodyPart)i, depthMod);
                                        maxItems--;
                                    }
                                    break;
                                }
                        }
                    }
                    int t = tile.X + tile.Y * w;
                    invalidTiles[t] = true;
                    npcPos[t] = (ushort)(index + 1);
                    NpcList[index] = npc;
                    index++;
                }
            }

            int potionCount = (int)(MathHelper.Lerp(5, 30, depthMod));
            int hpCount = potionCount / 2;
            while (potionCount > 0)
            {
                int x = Main.GetRandom(1, level.width);
                int y = Main.GetRandom(1, level.height);
                var point = GetClosestEmptyTile(x, y, 4, invalidTiles, level.width);
                if (point != Point.Zero)
                {
                    int tile = point.X + point.Y * level.width;
                    if (hpCount > 0)
                    {
                        potionPos[tile] = 1; // potion of health
                        hpCount--;
                    }
                    else
                        potionPos[tile] = 2; // potion of speed
                    invalidTiles[tile] = true;
                    potionCount--;
                }
            }

            Point pTile = Point.Zero;
            while (pTile == Point.Zero)
            {
                int x = Main.GetRandom(1, level.width);
                int y = Main.GetRandom(1, level.height);
                pTile = GetClosestEmptyTile(x, y, 8, invalidTiles, level.width);
            }
            MovePlayer(pTile.X, pTile.Y);
            player.TransAmt = 1f;

            Point stairTile = Point.Zero;
            while (stairTile == Point.Zero)
            {
                int x = Main.GetRandom(1, level.width);
                int y = Main.GetRandom(1, level.height);
                stairTile = GetClosestEmptyWithPadding(x, y, 8, invalidTiles, level.width);
                if (stairTile != Point.Zero)
                {
                    var path = AStar.GetAPath(pTile, stairTile, validTiles, level.width);
                    if (path.Count == 0)
                        stairTile = Point.Zero;
                }
            }
            int stile = stairTile.X + stairTile.Y * level.width;
            if (DungeonDepth < MaxDepth) 
                specialTiles[stile] = (byte)SpecialTile.Stairs;
            else
                specialTiles[stile] = (byte)SpecialTile.Treasure;
            invalidTiles[stile] = true;

            TextArea text = StatusBar.FindChild("aps", false) as TextArea;
            text.Text = player.APs.ToString();
            text = StatusBar.FindChild("turn", false) as TextArea;
            text.Text = Turn.ToString();
            text = StatusBar.FindChild("depth", false) as TextArea;
            text.Text = String.Format("{0} / {1}", DungeonDepth, MaxDepth);
            text = StatusBar.FindChild("npc", false) as TextArea;
            text.Text = NpcList.Length.ToString(); 
            
            guiControl.Remove(noticeBox);            
            generateThread = null;
        }

        Item GenerateItem(BodyPart bodyPart, float mod)
        {
            var sheet = CharSheets[(int)bodyPart];
            switch (bodyPart)
            {
                case BodyPart.Weapon:
                    {
                        Weapon item = new Weapon() { BodyType = BodyPart.Weapon };
                        int r = Main.GetRandom(0, 3);
                        if (r == 0)
                        {
                            item.AttackType = AttackType.Melee;
                            item.Damage = (byte)Main.GetRandom((int)MathHelper.Lerp(6, 20 + 5 * Difficulty, mod), (int)MathHelper.Lerp(10, 25 + 10 * Difficulty, mod));
                            item.Range = 1;
                            item.gID = (ushort)Main.GetRandom(MeleeFirstGID, MeleeFirstGID + MeleeCount);
                            item.Name = String.Format("{0} {1}", Adjectives[Main.GetRandom(0, Adjectives.Length)], SwordString[Main.GetRandom(0, SwordString.Length)]);
                        }
                        else if (r == 1)
                        {
                            item.AttackType = AttackType.Magic;
                            item.Damage = (byte)Main.GetRandom((int)MathHelper.Lerp(5, 15 + 5 * Difficulty, mod), (int)MathHelper.Lerp(8, 20 + 8 * Difficulty, mod));
                            item.Range = (byte)Main.GetRandom((int)MathHelper.Lerp(2, 3, mod), (int)MathHelper.Lerp(3, 6, mod));
                            item.gID = (ushort)Main.GetRandom(MagicFirstGID, MagicFirstGID + MagicCount);
                            item.Name = String.Format("{0} staff", Adjectives[Main.GetRandom(0, Adjectives.Length)]);
                        }
                        else
                        {
                            item.AttackType = AttackType.Ranged;
                            item.Damage = (byte)Main.GetRandom((int)MathHelper.Lerp(4, 15 + 5 * Difficulty, mod), (int)MathHelper.Lerp(8, 20 + 8 * Difficulty, mod));
                            item.Range = (byte)Main.GetRandom((int)MathHelper.Lerp(2, 4, mod), (int)MathHelper.Lerp(4, 7, mod));
                            item.gID = (ushort)Main.GetRandom(RangedFirstGID, RangedFirstGID + RangedCount);
                            item.Name = String.Format("{0} {1}", Adjectives[Main.GetRandom(0, Adjectives.Length)], BowString[Main.GetRandom(0, BowString.Length)]);
                        }
                        return item;
                    }
                case BodyPart.Shield:
                    {

                        Armor shield = new Armor() { BodyType = bodyPart };
                        shield.gID = (ushort)Main.GetRandom(sheet.firstGID, sheet.firstGID + sheet.elementCount);
                        int armorValue = Main.GetRandom((int)MathHelper.Lerp(1, 10, mod), (int)MathHelper.Lerp(3, 15, mod)); 
                        while (armorValue > 0)
                        {
                            int defType = Main.GetRandom(0, 3);
                            byte defAmt = (byte)Main.GetRandom(1, armorValue + 1);
                            shield.Defense[defType] += defAmt;
                            armorValue -= defAmt;
                        }
                        shield.Name = String.Format("{0} {1}", Adjectives[Main.GetRandom(0, Adjectives.Length)], ShieldString[Main.GetRandom(0, ShieldString.Length)]);

                        return shield;
                    }
                case BodyPart.Cape:
                    {
                        return new Equipment { Name = "a Cape", BodyType = BodyPart.Cape, gID = (ushort)Main.GetRandom(sheet.firstGID, sheet.firstGID + sheet.elementCount) };
                    }
                default:
                    {
                        Armor item = new Armor() { BodyType = bodyPart };
                        item.gID = (ushort)Main.GetRandom(sheet.firstGID, sheet.firstGID + sheet.elementCount);
                        int armorValue = Main.GetRandom((int)MathHelper.Lerp(1, 10, mod), (int)MathHelper.Lerp(3, 15, mod));
                        while (armorValue > 0)
                        {
                            int defType = Main.GetRandom(0, 3);
                            byte defAmt = (byte)Main.GetRandom(1, armorValue + 1);
                            item.Defense[defType] += defAmt;
                            armorValue -= defAmt;
                        }
                        item.Name = String.Format("{0} {1}", Adjectives[Main.GetRandom(0, Adjectives.Length)], item.BodyType.ToString());
                        return item;
                    }
            }
        }

        Layer[] GenerateLayers(int w, int h)
        {
            BitArray floor = new BitArray(w * h);

            int roomcount = (w * h) / 100;
            int maxfailcount = roomcount * 2;
            List<Rectangle> roomList = new List<Rectangle>();
            while (roomcount > 0 && maxfailcount > 0)
            {
                int rx = Main.GetRandom(2, w - 6);
                int ry = Main.GetRandom(2, h - 6);
                int rw = Main.GetRandom(4, 9);
                int rh = Main.GetRandom(4, 9);
                if (PlaceRoom(floor, w, rx, ry, rw, rh))
                {
                    roomList.Add(new Rectangle(rx, ry, rw, rh));
                    roomcount--;
                }
                else
                    maxfailcount--;
            }
            Rectangle[] rooms = roomList.ToArray();
            ConnectRooms(floor, w, rooms);

            UInt16[] floorTiles = new UInt16[w * h];
            for (int i = 0; i < floor.Length; i++)
                if (floor[i])
                    floorTiles[i] = 16; // set floor tile gID
            Layer floorLayer = new Layer { type = LayerType.Floor, gIDs = floorTiles };

            UInt16[] wallTiles = PlaceWalls(floorTiles, w);
            Layer wallLayer = new Layer { type = LayerType.Collision, gIDs = wallTiles };

            return new Layer[] { floorLayer, wallLayer };
        }

        bool PlaceRoom(BitArray floor, int MapW, int Rx, int Ry, int Rw, int Rh)
        {
            int h = floor.Length / MapW;
            int endX = Rx + Rw;
            int endY = Ry + Rh;
            if (Rx < 0 || endX >= MapW - 1 || Ry < 0 || endY >= h - 1)
                return false;
            BitArray buffer = new BitArray(floor.Length);
            for (int yF = Ry; yF < endY; yF++)
            {
                for (int xF = Rx; xF < endX; xF++)
                {
                    int targetTile = xF + yF * MapW;
                    if (floor[targetTile])
                        return false;
                    if (xF + 2 < MapW && floor[targetTile + 2])
                        return false;
                    if (xF - 2 >= 0 && floor[targetTile - 2])
                        return false;
                    buffer[targetTile] = true;
                }
            }
            floor = floor.Or(buffer);
            return true;
        }

        void ConnectRooms(BitArray floor, int w, Rectangle[] rooms)
        {
            int h = floor.Length / w;
            Point[] doors = new Point[rooms.Length];
            BitArray buffer = new BitArray(floor.Length);
            for (int r = 0; r < rooms.Length; r++)
            {
                int side = Main.GetRandom(0, 4);
                int x, y;
                if (side == 0)
                {
                    x = Main.GetRandom(rooms[r].Left + 1, rooms[r].Right);
                    y = rooms[r].Top - 1;
                }
                else if (side == 2)
                {
                    x = Main.GetRandom(rooms[r].Left + 1, rooms[r].Right - 1);
                    y = rooms[r].Bottom;
                }
                else if (side == 1)
                {
                    x = rooms[r].Right;
                    y = Main.GetRandom(rooms[r].Top + 1, rooms[r].Bottom - 1);
                }
                else
                {
                    x = rooms[r].Left - 1;
                    y = Main.GetRandom(rooms[r].Top + 1, rooms[r].Bottom - 1);
                }
                doors[r] = new Point(x, y);
            }
            for (int r = 0; r < doors.Length; r++)
            {
                Point start = doors[r];
                for (int r2 = 0; r2 < rooms.Length; r2++)
                {
                    Point end = doors[r2];
                    var path = AStar.GetAPath(start, end, floor, w);
                    if (path.Count != 0)
                    {
                        foreach (Point p in path)
                        {
                            int tile = p.X + p.Y * w;
                            buffer[tile] = true;
                        }
                        break;
                    }
                }
            }
            floor = floor.Or(buffer);
        }

        UInt16[] PlaceWalls(UInt16[] floor, int w)
        {
            UInt16[] wall = new UInt16[floor.Length];
            UInt16 top = 1;
            UInt16[] topVariant = new UInt16[] { 2, 3, 4, 5, 6, 7 };
            UInt16 topleft = 8;
            UInt16 topright = 9;
            UInt16 topboth = 10;
            UInt16 left = 11;
            UInt16 right = 12;
            UInt16 both = 13;
            UInt16 cornerleft = 14;
            UInt16 cornerright = 15;

            int h = floor.Length / w;

            // Place top/bottom left/right
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int tile = x + y * w;
                    UInt16 gid = floor[tile];
                    if (gid != 0)
                    {
                        if (x != w - 1 && floor[tile + 1] == 0 && wall[tile + 1] == 0)
                            wall[tile + 1] = right;
                        if (x != 0 && floor[tile - 1] == 0)
                        {
                            if (wall[tile - 1] == 0)
                                wall[tile - 1] = left;
                            else if (wall[tile - 1] == right)
                                wall[tile - 1] = both;
                        }
                        if (y != 0 && floor[tile - w] == 0)
                            wall[tile - w] = top;
                        if (y != h - 1 && floor[tile + w] == 0)
                            wall[tile + w] = top;
                    }
                }
            }

            // Place end blocks
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int tile = x + y * w;
                    if (wall[tile] == top)
                    {
                        if (x != 0 && floor[tile - 1] == 0)
                        {
                            if (wall[tile - 1] == 0)
                            {
                                if (y != h - 1 && wall[tile - 1 + w] != 0)
                                    wall[tile - 1] = left;
                                else
                                    wall[tile - 1] = cornerleft;
                            }
                            else
                                wall[tile - 1] = top;
                        }
                        if (x != w - 1 && floor[tile + 1] == 0)
                        {
                            if (wall[tile + 1] == 0)
                            {
                                if (y != h - 1 && wall[tile + 1 + w] != 0)
                                    wall[tile + 1] = right;
                                else
                                    wall[tile + 1] = cornerright;
                            }
                            else
                                wall[tile + 1] = top;
                        }

                    }
                }
            }
            // replace some walls
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int tile = x + y * w;
                    if (wall[tile] == top)
                    {
                        if (y != h - 1)
                        {
                            if (wall[tile + w] == left || wall[tile + w] == cornerleft)
                                wall[tile] = topleft;
                            else if (wall[tile + w] == right || wall[tile + w] == cornerright)
                                wall[tile] = topright;
                            else if (wall[tile + w] == both)
                                wall[tile] = topboth;
                            else if (wall[tile + w] == top)
                            {
                                if ((wall[tile + w + 1] == 0 && wall[tile + w - 1] == 0) || (wall[tile + 1] == 0 && wall[tile + -1] == 0))
                                    wall[tile] = topboth;
                                else if (wall[tile + w + 1] == 0 || wall[tile + 1] == 0)
                                    wall[tile] = topleft;
                                else if (wall[tile + w - 1] == 0 || wall[tile - 1] == 0)
                                    wall[tile] = topright;

                            }

                        }
                        if (wall[tile] == top && Main.GetRandom(0, 100) < 30)
                            wall[tile] = topVariant[Main.GetRandom(0, topVariant.Length)];
                    }
                }
            }


            return wall;
        } 
        #endregion

        void UpdateLOS()
        {
            visible = new BitArray(level.width * level.height);
            //UInt16[] walls = level.layers.First(l => l.type == LayerType.Collision).gIDs;
            BitArray collision = level.GetCollisionBitArr();
            //BitArray collision = walls.ToBitArray();

            int distance = 10;
            int diagonal = (int)(distance * 1.4f);
            for (int y = player.Tile.Y - distance; y <= player.Tile.Y + distance; y++)
            {
                if (y < 0 || y >= level.height)
                    continue;
                for (int x = player.Tile.X - distance; x <= player.Tile.X + distance; x++)
                {
                    if (x < 0 || x >= level.width)
                        continue;
                    int tile = x + y * level.width;
                    int xDist = Math.Abs(player.Tile.X - x);
                    int yDist = Math.Abs(player.Tile.Y - y);
                    if (xDist + yDist <= diagonal)
                    {
                        if (HasLOS(player.Tile, new Point(x,y), collision))
                            visible[tile] = true;
                    }
                }
            }
        }
        bool HasLOS(Point start, Point end, BitArray collision)
        {
            var tiles = GetPointsOnLine(start.X, start.Y, end.X, end.Y).ToArray();
            for (int i = 0; i < tiles.Length-1; i++)
            {
                int tile = tiles[i].X + tiles[i].Y * level.width;
                if (collision[tile] && tiles[i] != end)
                    return false;
            }
            return true;
        }
        public static IEnumerable<Point> GetPointsOnLine(int x0, int y0, int x1, int y1)
        {
            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            if (steep)
            {
                int t;
                t = x0; // swap x0 and y0
                x0 = y0;
                y0 = t;
                t = x1; // swap x1 and y1
                x1 = y1;
                y1 = t;
            }
            if (x0 > x1)
            {
                int t;
                t = x0; // swap x0 and x1
                x0 = x1;
                x1 = t;
                t = y0; // swap y0 and y1
                y0 = y1;
                y1 = t;
            }
            int dx = x1 - x0;
            int dy = Math.Abs(y1 - y0);
            int error = dx / 2;
            int ystep = (y0 < y1) ? 1 : -1;
            int y = y0;
            for (int x = x0; x <= x1; x++)
            {
                yield return (steep ? new Point(y, x) : new Point(x, y));
                error = error - dy;
                if (error < 0)
                {
                    y += ystep;
                    error += dx;
                }
            }
            yield break;
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // Pause all Updates if game window is not focused or if pause menu is open. 
            // Drawing still gets done and GUIcontroller will still get updated by Main, so buttons in menu will work.
            if (!Game.IsActive)
            {
                if (PauseMenu == null)
                    ToggleMenu();
                return;
            }
            else if (PauseMenu != null)
                return;

            HandleInput(gameTime);

            if (popup != null)
            {
                popup.Update();
                if (popup.Frame == null)
                    popup = null;
            }
            if (npcinfo != null)
            {
                npcinfo.Update();
                if (npcinfo.Host == null)
                    npcinfo = null;
            }

            if (generateThread == null && level != null)
            {
                float dT = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                try
                {
                    foreach (NPC npc in NpcList)
                    {
                        if (npc == null)
                            continue;
                        npc.Update(gameTime);
                    }
                    player.Update(gameTime);
                    if (ActiveProjectile != null)
                    {
                        ActiveProjectile.Update(gameTime);
                        if (!ActiveProjectile.Active)
                        {
                            if (ActiveProjectile.Owner == -1) // player's projectile
                            {
                                player.State = NpcState.Done;
                                Point t = new Point((int)(ActiveProjectile.EndPos.X / tileW) + startX, (int)(ActiveProjectile.EndPos.Y / tileH) + startY);
                                int tile = t.X + t.Y * level.width;
                                ushort npcID = npcPos[tile];
                                if (npcID != 0) // SHOULD always be true
                                {
                                    NPC npc = NpcList[npcID - 1];
                                    DoDmgToNPC(ActiveProjectile.OriginItem, npc);
                                }
                                ActiveProjectile = null;
                                if (player.APs == 0)
                                    NPCsTurn();
                                else
                                    UpdateMarkers();
                            }
                            else // npc's projectile
                            {
                                NpcList[ActiveProjectile.Owner].State = NpcState.Done;
                                DoDmgToPlayer(ActiveProjectile.OriginItem);
                                ActiveProjectile = null;
                            }
                        }
                    }
                    if (DmgText != null)
                    {
                        DmgText.Update(gameTime);
                        if (DmgText.TransAmt == 1)
                            DmgText = null;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                int ulistLen = NpcUpdateList.Length;
                if (ulistLen != 0)
                {
                    NPC npc = NpcUpdateList[0];
                    if (npc.State == NpcState.Idle) // npc ready for order
                    {
                        Weapon weapon = npc.Equipped[(int)BodyPart.Weapon] as Weapon;
                        if (WeaponCanHit(weapon, npc.Tile, player.Tile)) // npc attacks player
                        {
                            npc.State = NpcState.Waiting;
                            npc.APs = 0;
                            ActiveProjectile = new Projectile(npc.Position, player.Position, npc.Index, weapon);
                        }
                        else // npc tries to move closer to player
                        {
                            int tileID = npc.Tile.X + npc.Tile.Y * level.width;
                            Point attackSpot = OptimalHitPos(npc);
                            if (attackSpot != Point.Zero) // move to attack position for 1AP 
                            {
                                int newtileID = attackSpot.X + attackSpot.Y * level.width;
                                npc.Tile = attackSpot;
                                npcPos[tileID] = 0;
                                npcPos[newtileID] = (ushort)(npc.Index + 1);
                                npc.APs -= 1;
                            }
                            else
                            {
                                BitArray npcs = npcPos.ToBitArray();
                                BitArray walls = level.GetCollisionBitArr();
                                BitArray collision = npcs.Or(walls).Or(specialTiles.ToBitArray());

                                List<Point> path = AStar.GetAPath(npc.Tile, player.Tile, collision, level.width);
                                int count = path.Count;
                                if (count != 0)
                                {
                                    if (count > 3) // SHOULD always be true
                                    {
                                        npc.Tile = path[count - 3]; // move 2 tiles
                                        npc.APs -= 2;
                                    }
                                    else
                                    {
                                        npc.Tile = path[count - 2]; // move 1 tile
                                        npc.APs -= 1;
                                    }
                                    int tile2 = npc.Tile.X + npc.Tile.Y * level.width;
                                    npcPos[tileID] = 0;
                                    npcPos[tile2] = (ushort)(npc.Index + 1);
                                    if (!visible[tileID] && !visible[tile2]) // if npc not visible, do instant transition
                                    {
                                        npc.TransAmt = 1f;
                                        npc.State = NpcState.Done;
                                    }
                                }
                                else // cant find path (other npcs blocking)
                                {
                                    path = AStar.GetAPath(npc.Tile, player.Tile, walls, level.width); // get a path without npc collision
                                    count = path.Count;
                                    Point moveTile = Point.Zero;
                                    int destTile = 0;
                                    if (count > 3)
                                    {
                                        destTile = path[count - 3].X + path[count - 3].Y * level.width;
                                        if (npcPos[destTile] != 0)
                                        {
                                            destTile = path[count - 2].X + path[count - 2].Y * level.width;
                                            if (npcPos[destTile] == 0)
                                                moveTile = path[count - 2];
                                        }
                                        else
                                            moveTile = path[count - 3];
                                    }

                                    if (moveTile != Point.Zero)
                                    {
                                        npc.Tile = moveTile;
                                        npcPos[destTile] = npcPos[tileID];
                                        npcPos[tileID] = 0;
                                        if (!visible[tileID] && !visible[destTile]) // if npc not visible, do instant transition
                                        {
                                            npc.TransAmt = 1f;
                                            npc.State = NpcState.Done;
                                        }
                                    }
                                    else 
                                        npc.State = NpcState.Done;                                    
                                    npc.APs = 0;
                                }
                            }
                        }
                    }

                    if (npc.APs == 0 && npc.State == NpcState.Done)// remove NPC from update list          
                    {
                        npc.APs = 2;
                        NPC[] newList = new NPC[ulistLen - 1];
                        for (int i = 0; i < ulistLen - 1; i++)
                            newList[i] = NpcUpdateList[i + 1];
                        NpcUpdateList = newList;
                        UpdateMiniMap(ref miniMap);
                        if (ulistLen - 1 == 0) // if no more npc's to update, it's player's turn
                            PlayersTurn();
                    }
                }
            }       
            base.Update(gameTime);
        }
        ushort[] GetArmorRating(NPC npc)
        {
            ushort[] result = new ushort[] { 0, 0, 0 };
            foreach(Item i in npc.Equipped)
            {
                Armor a = i as Armor;
                if (a != null)
                {
                    result[0] += a.Defense[0];
                    result[1] += a.Defense[1];
                    result[2] += a.Defense[2];
                }
            }
            return result;
        }
        Point OptimalHitPos(NPC npc, int range = 1)
        {
            Weapon weapon = npc.Equipped[(int)BodyPart.Weapon] as Weapon;
            if (weapon == null)
                return Point.Zero;

            BitArray npcs = npcPos.ToBitArray();
            BitArray walls = level.GetCollisionBitArr();
            BitArray collision = npcs.Or(walls).Or(specialTiles.ToBitArray());
            for (int yMod = -1 * range; yMod <= range; yMod++)
            {
                for (int xMod = -1 * range; xMod <= range; xMod++)
                {
                    if (Math.Abs(yMod) + Math.Abs(xMod) > range)
                        continue;
                    Point checkTile = new Point(npc.Tile.X + xMod, npc.Tile.Y + yMod);
                    int tileID = checkTile.X + checkTile.Y * level.width;
                    if (collision[tileID])
                        continue;
                    if (WeaponCanHit(weapon, checkTile, player.Tile, collision))
                        return checkTile; 
                }
            }
            return Point.Zero;
        }
        bool WeaponCanHit(Weapon weapon, Point start, Point end, BitArray collision = null)
        {
            if (start == end)
                return true;

            int diffX = Math.Abs(start.X - end.X);
            int diffY = Math.Abs(start.Y - end.Y);

            if (diffX <= 1 && diffY <= 1) // within range for all weapon types
                return true;

            if (weapon.AttackType == AttackType.Ranged)
            {
                if ((diffX == 0 && diffY <= weapon.Range) || (diffY == 0 && diffX <= weapon.Range)) // same column or row and within range
                {
                    int modY = 1;
                    if (diffY == 0)
                        modY = 0;
                    else if (end.Y < start.Y)
                        modY = -1;

                    int modX = 1;
                    if (diffX == 0)
                        modX = 0;
                    else if (end.X < start.X)
                        modX = -1;

                    if (collision == null)
                    {
                        BitArray npcs = npcPos.ToBitArray();
                        BitArray walls = level.GetCollisionBitArr();
                        collision = npcs.Or(walls).Or(specialTiles.ToBitArray());
                    }
                    int diff = diffX;
                    if (diffX == 0)
                        diff = diffY;
                    for (int i = 1; i < diff; i++)
                    {
                        int t = (start.X + modX * i) + (start.Y + modY * i) * level.width;
                        if (collision[t])
                            return false;
                    }
                    return true;
                }
            }
            else if (weapon.AttackType == AttackType.Magic)
            {
                if (diffX == diffY && diffX <= weapon.Range) // target is on a diagonal tile and within range
                {
                    int modX = 1;
                    int modY = 1;
                    if (end.X < start.X)
                        modX = -1;
                    if (end.Y < start.Y)
                        modY = -1;

                    if (collision == null)
                    {
                        BitArray npcs = npcPos.ToBitArray();
                        BitArray walls = level.GetCollisionBitArr();
                        collision = npcs.Or(walls);
                    }
                    for (int i = 1; i < diffX; i++)
                    {
                        int t = (start.X + modX * i) + (start.Y + modY * i) * level.width;
                        if (collision[t])
                            return false;
                    }
                    return true;
                }
            }
            return false;
        }
        void HandleInput(GameTime gameTime)
        {
            oldKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();
            oldMouseState = mouseState;
            mouseState = Mouse.GetState();
            if (oldKeyboardState.IsKeyDown(Keys.Escape) && keyboardState.IsKeyUp(Keys.Escape))
                ToggleMenu();
            if (generateThread != null || level == null || mouseState.X < 0 || mouseState.Y < 0 || mouseState.X > GraphicsDevice.Viewport.Width || mouseState.Y > GraphicsDevice.Viewport.Height)
                return;
            
            if (oldKeyboardState.IsKeyDown(Keys.Subtract) && keyboardState.IsKeyUp(Keys.Subtract) && tileW > 16)
            {
                tileW -= 4;
                tileH -= 4;
                CenterCameraOnTile(player.Tile.X, player.Tile.Y);
            }
            else if (oldKeyboardState.IsKeyDown(Keys.Add) && keyboardState.IsKeyUp(Keys.Add) && tileW < 128)
            {
                tileW += 4;
                tileH += 4;
                CenterCameraOnTile(player.Tile.X, player.Tile.Y);
            }

            /*if (oldKeyboardState.IsKeyDown(Keys.Left) && keyboardState.IsKeyUp(Keys.Left))
                startX -= 1;
            else if (oldKeyboardState.IsKeyDown(Keys.Right) && keyboardState.IsKeyUp(Keys.Right))
                startX += 1;
            if (oldKeyboardState.IsKeyDown(Keys.Up) && keyboardState.IsKeyUp(Keys.Up))
                startY -= 1;
            else if (oldKeyboardState.IsKeyDown(Keys.Down) && keyboardState.IsKeyUp(Keys.Down))
                startY += 1;*/

            // toggle inventory
            if (oldKeyboardState.IsKeyDown(Keys.I) && keyboardState.IsKeyUp(Keys.I))
                ToggleInventory();

            // minimap
            if (oldKeyboardState.IsKeyDown(Keys.M) && keyboardState.IsKeyUp(Keys.M))
                ToggleMap();


            #region inventory Drag & Drop
            if (grabbedControl != null)
            {
                if (NpcUpdateList.Length != 0) // not player's turn, re-draw inventory               
                    UpdateInventory();                
                else if (mouseState.LeftButton == ButtonState.Pressed) // drag
                {
                    grabbedControl.Position = new Rectangle(grabbedControl.Position.X + mouseState.X - oldMouseState.X, grabbedControl.Position.Y + mouseState.Y - oldMouseState.Y, grabbedControl.Position.Width, grabbedControl.Position.Height);
                }
                else // drop
                {
                    List<Control> controls = guiControl.ControlsAt(mouseState.X, mouseState.Y);
                    Rectangle oldPos = (Rectangle)grabbedControl.Tag;
                    int grabbedIndex = 0;
                    string[] grabbedSplit = grabbedControl.Name.Split('_');
                    int.TryParse(grabbedSplit[1], out grabbedIndex);
                    bool fromEq = (grabbedSplit[0] == "eq") ? true : false;
                    foreach (Control c in controls) // cycle trough controls under mouse position
                    {
                        if (c == grabbedControl) // skip the grabbed item sprite
                            continue;
                        if (c.Name == "trash") // trash the item
                        {
                            if (fromEq)
                                player.Equipped[grabbedIndex] = null;
                            else
                                player.Inventory[grabbedIndex] = null;
                            grabbedControl = null;
                            break;
                        }
                        string[] split = c.Name.Split('_');
                        int len = split.Length;
                        if (len < 2) // not an inventory item
                            break;
                        int index = 0; // index of inventory or equipment slot
                        if (!int.TryParse(split[1], out index))
                            break;
                        if (split[0] == "item") // we're hovering over inventory item (slot or marker)
                        {
                            Control itemC;
                            if (len == 2)  // we're hovering over inventory item                            
                                itemC = c;
                            else // we're hovering over inventory marker                                
                                itemC = c.GetChildAt(0); //first child should be item sprite or null     


                            if (itemC != null && fromEq) // we're dropping on an inventory item from the equipped slot. Check for compatibility
                            {
                                Item item = player.Inventory[index];
                                Equipment eq = item as Equipment;
                                if ((int)eq.BodyType != grabbedIndex) // if droping equiped item on non-compatible item, break
                                    break;
                            }

                            //swap inventory
                            Item tempI = player.Inventory[index];
                            var fromArray = player.Inventory;
                            if (fromEq)
                                fromArray = player.Equipped;
                            player.Inventory[index] = fromArray[grabbedIndex];
                            fromArray[grabbedIndex] = tempI;

                            if (fromEq && grabbedIndex == (int)BodyPart.Weapon) // update markers to suit new weapon
                                UpdateMarkers();
                            break;
                        }
                        else if (split[0] == "eq")// we're hovering over equipment item (slot or marker)
                        {
                            Sprite markerC = null;
                            Control itemC = null;
                            if (len == 2) // we're hovering over equipment item
                            {
                                markerC = c.Parent as Sprite; // parent is marker
                                itemC = c;
                            }
                            else if (split[2] == "marker") // we're hovering over equipment marker
                            {
                                markerC = c as Sprite;
                                itemC = c.GetChildAt(0); // item is 1st child or null
                            }
                            if (markerC != null && markerC.Source == markers.GetSourceRect(4)) // marker is green (valid)
                            {                                
                                if (itemC == grabbedControl) // dropped on the original slot, break.
                                    break;                          

                                //swap inventory
                                Item item = player.Inventory[grabbedIndex]; // the dropped item
                                player.Inventory[grabbedIndex] = player.Equipped[index]; // put previously equipped item (or null) in inventory slot
                                player.Equipped[index] = item; // put dropped item in equipment slot

                                if (item.GetType() == typeof(Weapon)) // update markers to suit new weapon
                                    UpdateMarkers();
                                break;
                            }
                        }
                    }
                    UpdateInventory(); // re-draw inventory screen
                }
            }
            #endregion


            Control TopControl = guiControl.TopControlAt(mouseState.X, mouseState.Y);
            if (TopControl == guiControl || TopControl.Name == "pophost")
            {
                int x = (oldMouseState.X / tileW) + startX;
                int y = (oldMouseState.Y / tileH) + startY;
                if (x >= 0 && x < level.width && y >= 0 && y < level.height)
                {
                    int tile = x + y * level.width;
                    if (oldMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                    {

                        ushort marker = markerPos[tile];
                        if (marker != 0 && player.State == NpcState.Idle && player.APs != 0)
                        {
                            if (marker == 1) //move marker
                                MovePlayer(x, y);
                            else if (marker == 7) // 2 ap's move;
                            {
                                player.APs--;
                                MovePlayer(x, y);
                            }
                            else if (marker == 5) //attack marker
                            {
                                NPC npc = NpcList[npcPos[tile] - 1];
                                ActiveProjectile = new Projectile(player.Position, npc.Position, -1, player.Equipped[(int)BodyPart.Weapon]);
                                player.State = NpcState.Waiting;
                                markerPos = new ushort[level.width * level.height];
                            }
                            player.APs--;

                            TextArea text = StatusBar.FindChild("aps", false) as TextArea;
                            text.Text = player.APs.ToString();
                        }
                    }
                    else if (npcPos[tile] != 0 && visible[tile] && npcinfo == null && oldMouseState.RightButton == ButtonState.Pressed && mouseState.RightButton == ButtonState.Released)
                    {
                        NPC npc = NpcList[npcPos[tile] - 1];
                        Rectangle tilerect = new Rectangle((x - startX)*tileW, (y - startY) * tileH, tileW, tileH);
                        Rectangle popuprect = new Rectangle(mouseState.X + 32, mouseState.Y -125, 200, 250);
                        npcinfo = new NPCPupUp(npc, guiControl, tilerect, popuprect, (int)GUIFrameStyle.Holy, "");
                        npcinfo.KeepMouseDistance = true;
                        npcinfo.MouseDistance = new Point(32, -125);
                        npcinfo.Frame.BorderW = 6;
                        npcinfo.Frame.BorderH = 6;
                        Rectangle inner = npcinfo.Frame.InnerRect;
                        int w2 = inner.Width / 2;
                        Weapon w = npc.Equipped[(int)BodyPart.Weapon] as Weapon;

                        var txt = guiControl.AddTextArea("hp", "Health:", new Rectangle(inner.X, inner.Y, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Right;
                        txt = guiControl.AddTextArea("hpval", npc.HP.ToString(), new Rectangle(inner.X + w2 + 8, inner.Y, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Left;

                        txt = guiControl.AddTextArea("weapon", "Weapon:", new Rectangle(inner.X, inner.Y + 32, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Right;
                        txt = guiControl.AddTextArea("weapon", w.AttackType.ToString(), new Rectangle(inner.X + w2 + 8, inner.Y + 32, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Left;

                        txt = guiControl.AddTextArea("dmg", "Damage:", new Rectangle(inner.X, inner.Y + 64, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Right;
                        txt = guiControl.AddTextArea("dmg", w.Damage.ToString(), new Rectangle(inner.X + w2 + 8, inner.Y + 64, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Left;

                        guiControl.AddTextArea("armor", "Armor rating", new Rectangle(inner.X, inner.Y + 96, inner.Width, 32), npcinfo.Frame);
                        var armor = GetArmorRating(npc);
                        txt = guiControl.AddTextArea("melee", "Melee", new Rectangle(inner.X, inner.Y + 128, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Right;
                        txt = guiControl.AddTextArea("melee", armor[0].ToString(), new Rectangle(inner.X + w2 + 8, inner.Y + 128, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Left;
                        txt = guiControl.AddTextArea("ranged", "Ranged", new Rectangle(inner.X, inner.Y + 160, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Right;
                        txt = guiControl.AddTextArea("ranged", armor[1].ToString(), new Rectangle(inner.X + w2 + 8, inner.Y + 160, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Left;
                        txt = guiControl.AddTextArea("magic", "Magic", new Rectangle(inner.X, inner.Y + 192, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Right;
                        txt = guiControl.AddTextArea("magic", armor[2].ToString(), new Rectangle(inner.X + w2 + 8, inner.Y + 192, w2, 32), npcinfo.Frame);
                        txt.Alignment = TextAlignment.Left;

                        npcinfo.Host.BringToFront();
                    }
                }
            }
        }
        void ToggleMenu()
        {
            if (PauseMenu == null)
            {
                PauseMenu = guiControl.AddFrame("pause", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 150, GraphicsDevice.Viewport.Height / 2 - 150, 300, 300));
                var txt = guiControl.AddTextArea("paused", "Paused", new Rectangle(PauseMenu.Position.X, PauseMenu.Position.Y + 16, PauseMenu.Position.Width, 64), PauseMenu);
                txt.Font = Main.fontFipps;
                txt.Color = Color.DarkGray;
                guiControl.AddButton("resume", "Resume", new Rectangle(PauseMenu.Position.X + 20, PauseMenu.Position.Bottom - 140, PauseMenu.Position.Width - 40, 50), OnButtonClick, 0, PauseMenu);
                guiControl.AddButton("exit", "Save & Exit", new Rectangle(PauseMenu.Position.X + 20, PauseMenu.Position.Bottom - 70, PauseMenu.Position.Width - 40, 50), OnButtonClick, 0, PauseMenu);
            }
            else
            {
                guiControl.RemoveChild(PauseMenu);
                PauseMenu = null;
            }
        }
        void ToggleMap()
        {
            if (miniMap == null || level == null)
                return;
            Control c = guiControl.FindChild("minimap", false);
            if (c == null)
            {
                var msgbox = guiControl.AddMsgBox("minimap", new Rectangle(0, 32, 384, 300), (int)GUIFrameStyle.Default, (int)GUIButtonStyle.Close, null, null);
                msgbox.TitleText = "Minimap";
                guiControl.AddSprite("minimap_texture", new Rectangle(msgbox.Position.X + 8, msgbox.Position.Y + 8, msgbox.Position.Width - 16, msgbox.Position.Height - 16), miniMap, msgbox);
            }
            else
                guiControl.Remove(c);
        }
        void UpdateInventory()
        {
            Control inv = guiControl.FindChild("inventory");
            if (inv != null)
            {
                grabbedControl = null;
                guiControl.Remove(inv);
                OpenInventory(inv.Position.X, inv.Position.Y);
            }
        }
        void ToggleInventory()
        {
            Control inv = guiControl.FindChild("inventory", false);
            if (inv != null)
            {
                guiControl.Remove(inv);
                grabbedControl = null;
            }
            else
                OpenInventory();
        }
        void OpenInventory(int posX = 32, int posY = 64)
        {
            MessageBox box = guiControl.AddMsgBox("inventory", new Rectangle(posX, posY, 640, 400), 0, (int)GUIButtonStyle.Close);
            box.TitleText = "Inventory";
            int spriteW = 80;
            int spriteH = 80;
            int tileW = 100;
            int tileH = 100;
            int x, y;
            for (int i = 0; i < player.Inventory.Length; i++)
            {
                x = box.Position.X + (i % 3) * tileW + tileW / 2 - spriteW / 2;
                y = box.Position.Y + (i / 3) * tileH + tileH / 2 - spriteH / 2;

                Sprite marker = guiControl.AddSprite(String.Format("item_{0}_marker", i), new Rectangle(x, y, spriteW, spriteH), markers.texture, box);
                marker.Source = markers.GetSourceRect(1);

                if (player.Inventory[i] != null)
                {
                    Equipment item = player.Inventory[i] as Equipment;
                    var spriteSheet = CharSheets[(byte)item.BodyType];
                    int w = spriteW;
                    int h = spriteW;
                    if (item.BodyType == BodyPart.Weapon)
                    {
                        x += spriteW / 4;
                        y += 4;
                        h -= 8;
                        w -= 8;
                    }
                    else if (item.BodyType == BodyPart.Feet || item.BodyType == BodyPart.Legs)
                    {
                        y -= spriteW / 4;
                    }
                    else if (item.BodyType == BodyPart.Head)
                    {
                        y += spriteW / 4;
                    }
                    else if (item.BodyType == BodyPart.Shield)
                    {
                        x -= spriteW / 4;
                        y += 4;
                        h -= 8;
                        w -= 8;
                    }
                    Rectangle pos = new Rectangle(x, y, w, h);
                    Sprite sprite = guiControl.AddSprite(String.Format("item_{0}", i), pos, spriteSheet.texture, marker);
                    sprite.Source = spriteSheet.GetSourceRect(item.gID);
                    sprite.Tag = item;
                    sprite.FocusChanged += inventoryItemFocus;
                    sprite.OnMouseOver += OnEquipmentMouseOver;
                }
            }


            int bodyW = 180;
            int bodyH = 240;
            int w34 = box.Position.Center.X + box.Position.Width / 4;
            int padding = 16;
            Rectangle bodyPos = new Rectangle(w34 - bodyW / 2 - 8, box.Position.Y + box.Position.Height / 2 - bodyH / 2, bodyW, bodyH);

            if (player.Equipped[(int)BodyPart.Cape] != null) //draw cape before body
            {
                TileCollection capes = CharSheets[(int)BodyPart.Cape];
                Sprite capeSprite = guiControl.AddSprite("cape", bodyPos, capes.texture, box);
                capeSprite.Source = capes.GetSourceRect(player.Equipped[(int)BodyPart.Cape].gID);
            }
            TileCollection bodySheet = CharSheets[(byte)BodyPart.Body];
            Sprite bodySprite = guiControl.AddSprite("body", bodyPos, bodySheet.texture, box);
            bodySprite.Source = bodySheet.GetSourceRect(player.Type);

            for (int i = 0; i < player.Equipped.Length; i++)
            {
                int slot = 0;
                switch ((BodyPart)i)
                {
                    case BodyPart.Cape: { slot = 0; break; }
                    case BodyPart.Head: { slot = 1; break; }
                    case BodyPart.Armor: { slot = 2; break; }
                    case BodyPart.Arms: { slot = 5; break; }
                    case BodyPart.Weapon: { slot = 6; break; }
                    case BodyPart.Shield: { slot = 8; break; }
                    case BodyPart.Legs: { slot = 11; break; }
                    case BodyPart.Feet: { slot = 10; break; }
                }
                x = box.Position.Center.X + (slot % 3) * (tileW + padding) + tileW / 2 - spriteW / 2 - padding / 2;
                y = box.Position.Y + (slot / 3) * tileH + tileH / 2 - spriteH / 2;
                Sprite slotmarker = guiControl.AddSprite(String.Format("eq_{0}_marker", i), new Rectangle(x, y, spriteW, spriteH), markers.texture, box);
                slotmarker.Source = markers.GetSourceRect(2);
                if (player.Equipped[i] != null)
                {
                    Equipment item = player.Equipped[i] as Equipment;
                    TileCollection partSheet = CharSheets[(byte)item.BodyType];
                    int w = spriteW;
                    int h = spriteW;
                    if (item.BodyType == BodyPart.Weapon)
                    {
                        x += spriteW / 4;
                        y += 4;
                        h -= 8;
                        w -= 8;
                    }
                    else if (item.BodyType == BodyPart.Feet || item.BodyType == BodyPart.Legs)
                    {
                        y -= spriteW / 4;
                    }
                    else if (item.BodyType == BodyPart.Head)
                    {
                        y += spriteW / 4;
                    }
                    else if (item.BodyType == BodyPart.Shield)
                    {
                        x -= spriteW / 4;
                        y += 4;
                        h -= 8;
                        w -= 8;
                    }
                    Rectangle pos = new Rectangle(x, y, w, h);
                    Sprite sprite = guiControl.AddSprite(String.Format("eq_{0}", i), new Rectangle(x, y, w, h), partSheet.texture, slotmarker);
                    sprite.Source = partSheet.GetSourceRect(item.gID);
                    sprite.Tag = item;
                    sprite.FocusChanged += inventoryItemFocus;
                    sprite.OnMouseOver += OnEquipmentMouseOver;
                    if (item.BodyType != BodyPart.Cape)
                    {
                        Sprite capeSprite = guiControl.AddSprite(String.Format("eq_{0}_big", i), bodyPos, partSheet.texture, box);
                        capeSprite.Source = partSheet.GetSourceRect(item.gID);
                    }
                }
            }

            var trash = guiControl.AddSprite("trash", new Rectangle(box.Position.Center.X, box.Position.Bottom - tileH + tileH / 2 - spriteH / 2, spriteW, spriteH), markers.texture, box);
            trash.Source = markers.GetSourceRect(6);
            guiControl.AddTextArea("trash", "Trash", trash.Position, trash);

        }
        void OnEquipmentMouseOver(Object sender, EventArgs args)
        {
            Control c = sender as Control;
            if (c == null)
                return;
            Equipment item = c.Tag as Equipment;
            if (item == null)
                return;
            popup = new PopUp(c, new Rectangle(mouseState.X + 16, mouseState.Y - 100, 150, 200), (int)GUIFrameStyle.Default, "");
            popup.Frame.BorderH = 6;
            popup.Frame.BorderW = 6;
            popup.KeepMouseDistance = true;
            popup.MouseDistance = new Point(16, -100);

            Rectangle inner = popup.Frame.InnerRect;
            var text = guiControl.AddTextArea("itemname", item.Name, new Rectangle(inner.X, inner.Y, inner.Width, 32), popup.Frame);
            text.Color = Color.DarkRed;
            Armor armor = item as Armor;
            Weapon weapon = item as Weapon;
            int curY = inner.Y + 40;
            int w2 = inner.Width / 2;
            if (armor != null)
            {
                text = guiControl.AddTextArea("defensetxt", "Armor ratings", new Rectangle(inner.X, curY, inner.Width, 24), popup.Frame);
                //text.Color = Color.DarkRed;
                curY += 32; 
                for (int i = 0; i < 3; i++)
                {
                    text = guiControl.AddTextArea("defensetxt", String.Format("{0}: {1}", ((AttackType)i).ToString(), armor.Defense[i]), new Rectangle(inner.X, curY, inner.Width, 24), popup.Frame);
                    //text.Color = Color.DarkRed;
                    curY += 32;                    
                }
            }
            else if (weapon != null)
            {
                text = guiControl.AddTextArea("dmgtext", String.Format("Damage: {0}", weapon.Damage), new Rectangle(inner.X, curY, inner.Width, 24), popup.Frame);
                curY += 32;
                text = guiControl.AddTextArea("dmgtext", String.Format("Range: {0}", weapon.Range), new Rectangle(inner.X, curY, inner.Width, 24), popup.Frame);              

            }
            
        }
        void inventoryItemFocus(object sender, EventArgs args)
        {
            Control c = sender as Control;
            if (c.Focused && grabbedControl == null && NpcUpdateList.Length == 0)
            {
                grabbedControl = c;
                c.Tag = c.Position;
                c.BringToFront();
                c.Parent.BringToFront();

                string[] split = c.Name.Split('_');

                int index = 0;
                if (int.TryParse(c.Name.Split('_')[1], out index)) // change marker of the correct equipment slot
                {
                    Equipment item = player.Inventory[index] as Equipment;
                    if (split[0] == "eq")
                        item = player.Equipped[index] as Equipment;
                    if (item != null)
                    {
                        var marker = guiControl.FindChild(String.Format("eq_{0}_marker", (int)item.BodyType)) as Sprite;
                        if (marker != null)
                            marker.Source = markers.GetSourceRect(4);
                    }
                }

            }
        }        
        void OnButtonClick(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null)
            {
                switch (b.Name)
                {
                    case "closeparent":
                        {
                            try
                            {                               
                                guiControl.RemoveChild(b.Parent);                                
                            }
                            catch { }
                            break;
                        }
                    case "exit":
                        {
                            ExitToTitle();
                            break;
                        }
                    case "exitnosave":
                        {
                            ExitToTitle(false);
                            break;
                        }
                    case "invicon":
                        {
                            ToggleInventory();
                            break;
                        }
                    case "mapicon":
                        {
                            ToggleMap();
                            break;
                        }
                    case "resume":
                    case "menuicon":
                        {
                            ToggleMenu();
                            break;
                        }
                    case "infopage":
                        {
                            int pagenum = (int)b.Tag;
                            ShowStartDialog(pagenum);
                            break;
                        }
                    case "start":
                        {
                            FrameBox frame = b.Parent as FrameBox;
                            guiControl.Remove(frame);
                            StartLvlGen();
                            break;
                        }
                }
            }
        }
        void UpdateMiniMap(ref Texture2D texture)
        {
            try
            {
                if (texture == null || texture.Width != level.width || texture.Height != level.height)
                    texture = new Texture2D(GraphicsDevice, level.width, level.height);
                Color[] colors = new Color[level.width * level.height];
                lock (_levelLocker)
                {
                    for (int y = 0; y < level.height; y++)
                    {
                        for (int x = 0; x < level.width; x++)
                        {
                            int tile = x + y * level.width;
                            colors[tile] = Color.Transparent;
                            if (uncovered[tile])
                            {
                                for (int l = 0; l < level.layers.Length; l++)
                                {
                                    Layer layer = level.layers[l];
                                    UInt16 gID = layer.gIDs[tile];
                                    if (gID != 0)
                                    {
                                        if (layer.type == LayerType.Collision)
                                            colors[tile] = Color.DarkGray;
                                        else if (layer.type == LayerType.Floor)
                                            colors[tile] = Color.LightGray;
                                    }
                                }
                                if (npcPos[tile] != 0)
                                    colors[tile] = Color.Red;
                                else if (potionPos[tile] != 0)
                                    colors[tile] = Color.Yellow;
                                else if (specialTiles[tile] != 0)
                                    colors[tile] = Color.Blue;
                            }
                          //  else if (specialTiles[tile] != 0)
                           //     colors[tile] = Color.Blue;
                        }
                    }
                    colors[player.Tile.X + player.Tile.Y * level.width] = Color.Green;
                }
                texture.SetData(colors);

                Control map = guiControl.FindChild("minimap", false);
                if (map != null)
                {
                    Sprite mapT = map.FindChild("minimap_texture") as Sprite;
                    if (mapT != null)
                        mapT.Texture = miniMap;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        void UpdateMarkers()
        {
            if (generateThread != null || level == null)
                return;
            markerPos = new UInt16[level.width * level.height];
            BitArray walls = level.GetCollisionBitArr();
            BitArray npcs = npcPos.ToBitArray();
            BitArray collision = npcs.Or(walls).Or(specialTiles.ToBitArray());

           // byte maxDistance = (byte)(2 * player.APs);
            byte maxDistance = 4;
            if (player.APs < 2)
                maxDistance = 2;
            for (int y = -1 * maxDistance; y <= maxDistance; y++)
            {
                int curY = player.Tile.Y + y;
                if (curY < 0 || curY >= level.height)
                    continue;
                for (int x = -1 * maxDistance; x <= maxDistance; x++)
                {
                    int curX = player.Tile.X + x;
                    if (curX < 0 || curX >= level.width)
                        continue;
                    int tile = curX + curY * level.width;
                    if (specialTiles[tile] != 0)  // disable collision if checking movement to special tile                  
                        collision[tile] = false;
                    var points = AStar.GetAPath(player.Tile, new Point(curX, curY), collision, level.width, maxDistance);
                    if (points.Count != 0)
                    {
                        int diffTotal = points.Count - 1;
                        if (diffTotal <= 2) // marker for 1 AP move
                            markerPos[tile] = 1;
                        else if (diffTotal <= maxDistance) // marker for 2 AP move
                            markerPos[tile] = 7;
                    }
                    if (specialTiles[tile] != 0) // re-enable collision for special tile                   
                        collision[tile] = true;
                }
            }

            Weapon weapon = player.Equipped[(int)BodyPart.Weapon] as Weapon;
            if (weapon != null)
            {
                foreach (NPC npc in NpcList)
                {
                    if (npc == null)
                        continue;
                    if (WeaponCanHit(weapon, player.Tile, npc.Tile, collision))
                    {
                        int tile = npc.Tile.X + npc.Tile.Y * level.width;
                        markerPos[tile] = 5;
                    }
                }
            }
        }
        void MovePlayer(int x, int y)
        {
            player.Tile = new Point(x, y);          //move player
            UpdateLOS();                            //update line of sight
            uncovered = uncovered.Or(visible);     //update uncovered bits
            UpdateMiniMap(ref miniMap);             //update minimap
            markerPos = new UInt16[level.width * level.height]; //clear markers
        }
        void OnPlayerState(Object sender, EventArgs e)
        {
            ObjectEventArgs args = e as ObjectEventArgs;
            NpcState oldState = (NpcState)args.Tag;
            if (oldState == NpcState.Transition) // player has finished moving
            {
                CenterCameraOnTile(player.Tile.X, player.Tile.Y);   // center camera  
                int tile = player.Tile.X + player.Tile.Y * level.width;

                if (potionPos[tile] != 0) // player has moved to potion tile
                {
                    Vector2 ppos = player.Position;
                    Vector2 epos = new Vector2(ppos.X, ppos.Y - tileH);
                    if (potionPos[tile] == 1) // potion of health
                    {
                        player.HP += 10;
                        if (player.HP > 100)
                            player.HP = 100;
                        HpText.Text = player.HP.ToString();
                        DmgText = new TransText("+10 HP", Color.Red, ppos, epos, 1500);
                    }
                    else // potion of speed
                    {
                        player.APs += 3;
                        DmgText = new TransText("+3 AP", Color.Yellow, ppos, epos, 1500);
                        TextArea text = StatusBar.FindChild("aps", false) as TextArea;
                        text.Text = player.APs.ToString();
                    }
                    potionPos[tile] = 0;
                    AudioMan.PotionSound();
                }
                if (groundItems[tile] != 0) // player has moved to ground item tile
                {
                    int itmIndx = groundItems[tile] - 1;
                    Item item = DroppedItems[itmIndx];
                    if (player.PutItemInInventory(item))
                    {
                        DroppedItems[itmIndx] = null;
                        groundItems[tile] = 0;
                        AudioMan.PickUpSound();
                        UpdateInventory();
                    }
                    else
                    {
                        Vector2 ppos = player.Position;
                        Vector2 epos = new Vector2(ppos.X, ppos.Y - tileH);
                        DmgText = new TransText("Inventory full!", Color.Yellow, ppos, epos, 2000);
                    }
                }
                if (specialTiles[tile] != 0) // player has moved on to a special tile
                {
                    SpecialTile sTile = (SpecialTile)specialTiles[tile];
                    if (sTile == SpecialTile.Stairs)
                    {
                        StartLvlGen();
                        player.APs = 2;
                    }
                    else if (sTile == SpecialTile.Treasure) // player has found the treasure, End game
                    {
                        EndGame(true);
                    }
                }

                if (player.APs == 0)// end of player's turn                
                    NPCsTurn();
                else
                {
                    UpdateMarkers();
                }
            }
            
        }

        void NPCsTurn()
        {
            List<IntInt> activeNPCs = new List<IntInt>();
            for (int i = 0; i < NpcList.Length; i++)
            {
                NPC npc = NpcList[i];
                if (npc == null)
                    continue;
                int tile = npc.Tile.X + npc.Tile.Y * level.width;
                if (!npc.SeenPlayer)
                {
                    if (visible[tile])
                    {
                        npc.SeenPlayer = true;
                        int distance = Math.Abs(player.Tile.X - npc.Tile.X) + Math.Abs(player.Tile.Y - npc.Tile.Y);
                        activeNPCs.Add(new IntInt(i, distance)); 
                    }
                }
                else
                {
                    int distance = Math.Abs(player.Tile.X - npc.Tile.X) + Math.Abs(player.Tile.Y - npc.Tile.Y);
                    activeNPCs.Add(new IntInt(i, distance));
                }
            }
            int len = activeNPCs.Count;
            if (len == 0) // no npcs to move, it's player's turn
                PlayersTurn();
            else
            {
                var sortedNPCs = activeNPCs.OrderBy(n => n.B); // order by distance, closest first
                NpcUpdateList = new NPC[len];
                for (int i = 0; i < len; i++)
                {
                    var e = sortedNPCs.ElementAt(i);
                    NpcUpdateList[i] = NpcList[e.A];
                }
            }
        }
        void PlayersTurn()
        {
            player.State = NpcState.Idle;
            player.APs = 2;
            Turn++;
            UpdateMarkers();
            TextArea text = StatusBar.FindChild("aps", false) as TextArea;
            text.Text = player.APs.ToString();
            text = StatusBar.FindChild("turn", false) as TextArea;
            text.Text = Turn.ToString();
        }
        void DoDmgToPlayer(Item item)
        {
            Weapon weapon = item as Weapon;
            if (weapon == null)
                return;
            int dmg = weapon.Damage;
            ushort[] armor = GetArmorRating(player);
            dmg -= armor[(int)weapon.AttackType]; // deduce the relevant defense rating
            if (dmg < 0)
                dmg = 0;
            
            if (dmg >= player.HP) // player dies
            {
                player.HP = 0;
                EndGame();
            }
            else 
                player.HP -= (byte)dmg;
            Vector2 startPos = player.Position;
            Vector2 endPos = new Vector2(startPos.X, startPos.Y - tileH);
            DmgText = new TransText(dmg.ToString(), Color.Red, startPos, endPos, 500);
            HpText.Text = player.HP.ToString();
            if (weapon.AttackType == AttackType.Magic)
                AudioMan.MagicSound();
            else
                AudioMan.HitSound();
        }
        void EndGame(bool win = false)
        {
            NpcUpdateList = new NPC[0];
            level = null;
            var frame = guiControl.AddFrame("endframe", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 150, GraphicsDevice.Viewport.Height / 2 - 100, 300, 200), (int)GUIFrameStyle.Holy);
            Rectangle inner = frame.InnerRect;
            if (win)
            {
                TextArea txt = guiControl.AddTextArea("endtext", "Congratulations", new Rectangle(inner.X, inner.Y, inner.Width, inner.Height / 4), frame);
                txt.Color = Color.DarkGreen;
                txt = guiControl.AddTextArea("endtext", "You have found the treasure!", new Rectangle(inner.X, inner.Y + inner.Height / 4, inner.Width, inner.Height / 4), frame);
                txt.Color = Color.DarkGreen;
            }
            else
            {
                TextArea deadtxt = guiControl.AddTextArea("endtext", "You have been killed!", new Rectangle(inner.X, inner.Y, inner.Width, inner.Height / 2), frame);
                deadtxt.Color = Color.DarkRed;
            }
            guiControl.AddButton("exitnosave", "Exit to main menu", new Rectangle(inner.Center.X - 100, inner.Bottom - 40, 200, 32), OnButtonClick, (int)GUIButtonStyle.Blue, frame);
        }
        void DoDmgToNPC(Item item, NPC npc)
        {
            Weapon weapon = item as Weapon;
            if (weapon == null)
                return;
            int dmg = weapon.Damage;
            ushort[] armor = GetArmorRating(npc);
            dmg -= armor[(int)weapon.AttackType]; // deduce the relevant defense rating
            if (dmg < 0)
                dmg = 0;

            if (dmg >= npc.HP) // npc dies
            {
                npc.HP = 0;
                NpcList[npc.Index] = null;
                int tile = npc.Tile.X + npc.Tile.Y * level.width;
                npcPos[tile] = 0;
                UpdateMarkers();
                AudioMan.DeathSound();

                List<int> eqIndx = new List<int>();
                for (int i = 0; i < npc.Equipped.Length; i++) // get all equipped items indexes
                    if (npc.Equipped[i] != null)
                        eqIndx.Add(i);
                if (eqIndx.Count != 0)
                    DropItem(npc.Equipped[eqIndx[Main.GetRandom(0, eqIndx.Count)]], tile); // drop random equiped item on ground


                TextArea text = StatusBar.FindChild("npc", false) as TextArea;
                int curNpcs = 0;
                if (text != null && int.TryParse(text.Text, out curNpcs))
                    text.Text = (curNpcs - 1).ToString();
            }
            else
                npc.HP -= (byte)dmg;
            Vector2 startPos = npc.Position;
            Vector2 endPos = new Vector2(startPos.X, startPos.Y - tileH);
            DmgText = new TransText(dmg.ToString(), Color.Red, startPos, endPos, 500);

            if (weapon.AttackType == AttackType.Magic)
                AudioMan.MagicSound();
            else
                AudioMan.HitSound();
        }
        void DropItem(Item item, int tile)
        {
            if (groundItems[tile] != 0)
                return;
            int index = -1;
            for (int i = 0; i < DroppedItems.Length; i++)
                if (DroppedItems[i] == null)
                {
                    index = i;
                    break;
                }
            if (index != -1)
            {
                DroppedItems[index] = item;
                groundItems[tile] = (byte)(index + 1);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (!Game.IsActive)
                return;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            if (generateThread == null && level != null)
            {
                int w6 = tileW / 6;
                int h4 = tileH / 4;

                lock (_levelLocker)
                {
                    int tileCountY = GraphicsDevice.Viewport.Height / tileH;
                    int tileCountX = GraphicsDevice.Viewport.Width / tileW;
                    int drawStage = 2;
                    while (drawStage > 0)
                    {
                        for (int y = 0; y <= tileCountY; y++)
                        {
                            int curY = y + startY;
                            if (curY < 0 || curY >= level.height)
                                continue;
                            for (int x = 0; x <= tileCountX; x++)
                            {
                                int curX = x + startX;
                                if (curX < 0 || curX >= level.width)
                                    continue;
                                int tile = curX + curY * level.width;
                                if (drawStage == 2) //tiles and markers stage
                                {
                                    if (!uncovered[tile])
                                        continue;
                                    Rectangle dest = new Rectangle(x * tileW, y * tileH, tileW, tileH);
                                    //tiles
                                    for (int l = 0; l < level.layers.Length; l++)
                                    {
                                        Layer layer = level.layers[l];
                                        UInt16 gID = layer.gIDs[tile];
                                        if (gID != 0)
                                        {
                                            Color c = Color.White;
                                            if (!visible[tile])
                                                c = Color.Gray;

                                            var tileSet = level.getTileSetFromGID(gID);
                                            if (tileSet != null)
                                                spriteBatch.Draw(tileSet.texture, dest, tileSet.GetSourceRect(gID), c, 0f, Vector2.Zero, SpriteEffects.None, 0.3f);
                                        }
                                    }
                                    //markers
                                    if (markerPos[tile] != 0)
                                        spriteBatch.Draw(markers.texture, dest, markers.GetSourceRect(markerPos[tile]), Color.White);
                                    //potions
                                    if (potionPos[tile] != 0)
                                        spriteBatch.Draw(Potions.texture, dest, Potions.GetSourceRect(potionPos[tile]), Color.White);
                                    //ground items
                                    if (groundItems[tile] != 0)
                                    {
                                        Equipment item = DroppedItems[groundItems[tile] - 1] as Equipment;
                                        var sheet = CharSheets[(int)item.BodyType];
                                        spriteBatch.Draw(sheet.texture, dest, sheet.GetSourceRect(item.gID), Color.White);
                                    }
                                    // special tiles
                                    if (specialTiles[tile] != 0)
                                        spriteBatch.Draw(SpecialSheet.texture, dest, SpecialSheet.GetSourceRect(specialTiles[tile]), Color.White);
                                    
                                }
                                else if (drawStage == 1) //npc stage
                                {
                                    //npcs
                                    if (npcPos[tile] != 0)
                                    {
                                        NPC npc = NpcList[npcPos[tile] - 1];
                                        if ((!uncovered[tile] || !visible[tile]) && npc.State != NpcState.Transition)
                                            continue;
                                        Vector2 pos = npc.Position;
                                        Rectangle dest = new Rectangle((int)pos.X - w6, (int)pos.Y - h4, tileW + 2 * w6, tileH + h4 - 4);
                                        if (npc.Equipped[(byte)BodyPart.Cape] == null)
                                        {
                                            TileCollection bodyset = CharSheets[(byte)BodyPart.Body];
                                            spriteBatch.Draw(bodyset.texture, dest, bodyset.GetSourceRect(npc.Type), Color.White);
                                        }
                                        for (int i = 0; i < npc.Equipped.Length; i++)
                                        {
                                            var item = npc.Equipped[i];
                                            if (item != null)
                                            {
                                                spriteBatch.Draw(CharSheets[i].texture, dest, CharSheets[i].GetSourceRect(item.gID), Color.White);
                                                if (i == (byte)BodyPart.Cape)
                                                {
                                                    TileCollection bodyset = CharSheets[(byte)BodyPart.Body];
                                                    spriteBatch.Draw(bodyset.texture, dest, bodyset.GetSourceRect(npc.Type), Color.White);
                                                }
                                            }
                                        }
                                        // npc ids
                                       // spriteBatch.DrawString(Main.font, npc.Index.ToString(), npc.Position, Color.DarkRed, 0f, Vector2.Zero, tileH / 32f, SpriteEffects.None, 0);
                                    }
                                }
                            }
                        }
                        drawStage--;
                    }
                }
                //player
                Vector2 pPos = player.Position;
                Rectangle pDest = new Rectangle((int)pPos.X - w6, (int)pPos.Y - h4, tileW + 2 * w6, tileH + h4 - 4);
                spriteBatch.Draw(CharSheets[(int)BodyPart.Body].texture, pDest, CharSheets[(int)BodyPart.Body].GetSourceRect(player.Type), Color.White);
                if (player.Equipped[(byte)BodyPart.Cape] == null)
                {
                    TileCollection bodyset = CharSheets[(byte)BodyPart.Body];
                    spriteBatch.Draw(bodyset.texture, pDest, bodyset.GetSourceRect(player.Type), Color.White);
                }
                for (int i = 0; i < player.Equipped.Length; i++)
                {
                    var item = player.Equipped[i];
                    if (item != null)
                    {
                        spriteBatch.Draw(CharSheets[i].texture, pDest, CharSheets[i].GetSourceRect(item.gID), Color.White);
                        if (i == (byte)BodyPart.Cape)
                        {
                            TileCollection bodyset = CharSheets[(byte)BodyPart.Body];
                            spriteBatch.Draw(bodyset.texture, pDest, bodyset.GetSourceRect(player.Type), Color.White);
                        }
                    }
                }

                if (ActiveProjectile != null)
                {
                    Weapon weapon = ActiveProjectile.OriginItem as Weapon;
                    if (weapon.AttackType == AttackType.Ranged)
                        spriteBatch.Draw(ArrowTileset.texture, ActiveProjectile.Position, ArrowTileset.GetSourceRect(ActiveProjectile.Direction), Color.White, 0f, Vector2.Zero, tileW / 32f, SpriteEffects.None, 0);
                    else if (weapon.AttackType == AttackType.Magic)
                        spriteBatch.Draw(Fireball, ActiveProjectile.Position, null, Color.White, 0f, Vector2.Zero, tileW / 32f, SpriteEffects.None, 0);
                    else
                        spriteBatch.Draw(SwordTileset.texture, ActiveProjectile.Position, SwordTileset.GetSourceRect(ActiveProjectile.Direction), Color.White, 0f, Vector2.Zero, tileW / 32f, SpriteEffects.None, 0);
                                   
                }
                if (DmgText != null)
                {
                    spriteBatch.DrawString(Main.font, DmgText.Text, DmgText.Position, DmgText.Color, 0f, Vector2.Zero, tileW / 32f, SpriteEffects.None, 0);
                }
            }
            spriteBatch.End();
        }


        public bool LoadGame()
        {
            try
            {
                using (FileStream stream = new FileStream(Path.Combine(Environment.CurrentDirectory, "savegame.rrs"), FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader reader = new BinaryReader(stream, System.Text.Encoding.UTF8))
                    {
                        //player
                        Turn = reader.ReadInt32();
                        player = new Player(this, "Player");
                        player.Type = reader.ReadByte();
                        player.State = (NpcState)reader.ReadByte();
                        player.HP = reader.ReadByte();
                        player.APs = reader.ReadByte();
                        player.Tile = new Point(reader.ReadInt32(), reader.ReadInt32());
                        for (int i = 0; i < player.Inventory.Length; i++)
                        {
                            Int16 type = reader.ReadInt16();
                            if (type == -1)
                                continue;
                            ushort gid = reader.ReadUInt16();
                            Item item;
                            if (type == 1)
                            {
                                Weapon w = new Weapon();
                                w.gID = gid;
                                w.BodyType = (BodyPart)reader.ReadByte();
                                w.AttackType = (AttackType)reader.ReadByte();
                                w.Damage = reader.ReadByte();
                                w.Range = reader.ReadByte();                                
                                item = w;
                            }
                            else if (type == 2)
                            {
                                Armor a = new Armor();
                                a.gID = gid;
                                a.BodyType = (BodyPart)reader.ReadByte();
                                a.Defense[0] = reader.ReadByte();
                                a.Defense[1] = reader.ReadByte();
                                a.Defense[2] = reader.ReadByte();
                                item = a;
                            }
                            else if (type == 3)
                                item = new Equipment() { BodyType = (BodyPart)reader.ReadByte(), gID = gid };
                            else
                            {
                                Console.WriteLine("item unknown type: {0}", type);
                                item = new Item() { gID = gid };
                            }
                            item.Name = reader.ReadString();
                            player.Inventory[i] = item;
                        }
                        for (int i = 0; i < player.Equipped.Length; i++)
                        {
                            Int16 type = reader.ReadInt16();
                            if (type == -1)
                                continue;
                            ushort gid = reader.ReadUInt16();
                            Item item;
                            if (type == 1)
                            {
                                Weapon w = new Weapon();
                                w.gID = gid;
                                w.BodyType = (BodyPart)reader.ReadByte();
                                w.AttackType = (AttackType)reader.ReadByte();
                                w.Damage = reader.ReadByte();
                                w.Range = reader.ReadByte();
                                item = w;
                            }
                            else if (type == 2)
                            {
                                Armor a = new Armor();
                                a.gID = gid;
                                a.BodyType = (BodyPart)reader.ReadByte();
                                a.Defense[0] = reader.ReadByte();
                                a.Defense[1] = reader.ReadByte();
                                a.Defense[2] = reader.ReadByte();
                                item = a;
                            }
                            else if (type == 3)
                                item = new Equipment() { BodyType = (BodyPart)reader.ReadByte(), gID = gid };
                            else
                            {
                                Console.WriteLine("item unknown type: {0}", type);
                                item = new Item() { gID = gid };
                            }
                            item.Name = reader.ReadString();
                            player.Equipped[i] = item;
                        }


                        //level
                        short lvlW = reader.ReadInt16();
                        short lvlH = reader.ReadInt16();
                        DungeonDepth = reader.ReadInt32();
                        MaxDepth = reader.ReadInt32();
                        int layerCount = reader.ReadInt32();
                        Layer[] layers = new Layer[layerCount];
                        for (int i = 0; i < layers.Length; i++)
                        {
                            Layer layer = new Layer();
                            layer.type = (LayerType)reader.ReadByte();
                            layer.gIDs = new ushort[lvlW * lvlH];
                            for (int j = 0; j < layer.gIDs.Length; j++)
                                layer.gIDs[j] = reader.ReadUInt16();
                            layers[i] = layer;
                        }
                        lock (_levelLocker)
                        {
                            level = new Level() { width = lvlW, height = lvlH, layers = layers, tileSets = new TileCollection[] { WallSet } };
                        }
                        //potion positions
                        potionPos = new byte[lvlW * lvlH];
                        for (int i = 0; i < potionPos.Length; i++)
                            potionPos[i] = reader.ReadByte();
                        //uncovered
                        uncovered = new BitArray(lvlW * lvlH);
                        visible = new BitArray(lvlW * lvlH);
                        for (int i = 0; i < uncovered.Length; i++)
                            uncovered[i] = reader.ReadBoolean();
                        //special tiles
                        specialTiles = new byte[lvlW * lvlH];
                        for (int i = 0; i < specialTiles.Length; i++)
                            specialTiles[i] = reader.ReadByte();
                        groundItems = new byte[lvlW * lvlH];

                        //npcs
                        npcPos = new ushort[lvlW * lvlH];
                        int npcCount = reader.ReadInt32();
                        NpcList = new NPC[npcCount];
                        for (ushort n = 0; n < npcCount; n++)
                        {
                            NPC npc = new NPC(this, n, new Point(reader.ReadInt32(), reader.ReadInt32()));
                            npc.Type = reader.ReadByte();
                            npc.HP = reader.ReadByte();
                            npc.APs = reader.ReadByte();
                            npc.MaxAP = reader.ReadByte();
                            npc.SeenPlayer = reader.ReadBoolean();
                            npc.State = (NpcState)reader.ReadByte();
                            for (int i = 0; i < npc.Equipped.Length; i++)
                            {
                                Int16 type = reader.ReadInt16();
                                if (type == -1)
                                    continue;
                                ushort gid = reader.ReadUInt16();
                                Item item;
                                if (type == 1)
                                {
                                    Weapon w = new Weapon();
                                    w.gID = gid;
                                    w.BodyType = (BodyPart)reader.ReadByte();
                                    w.AttackType = (AttackType)reader.ReadByte();
                                    w.Damage = reader.ReadByte();
                                    w.Range = reader.ReadByte();
                                    item = w;
                                }
                                else if (type == 2)
                                {
                                    Armor a = new Armor();
                                    a.gID = gid;
                                    a.BodyType = (BodyPart)reader.ReadByte();
                                    a.Defense[0] = reader.ReadByte();
                                    a.Defense[1] = reader.ReadByte();
                                    a.Defense[2] = reader.ReadByte();
                                    item = a;
                                }
                                else if (type == 3)
                                    item = new Equipment() { BodyType = (BodyPart)reader.ReadByte(), gID = gid };
                                else
                                {
                                    Console.WriteLine("item unknown type: {0}", type);
                                    item = new Item() { gID = gid };
                                }
                                item.Name = reader.ReadString();
                                npc.Equipped[i] = item;
                            }
                            int tile = npc.Tile.X + npc.Tile.Y * lvlW;
                            npcPos[tile] = (ushort)(npc.Index + 1);
                            NpcList[n] = npc;
                        }                         
                        
                    }
                }
                player.StateChanged += OnPlayerState;
                UpdateLOS();
                UpdateMarkers();
                CenterCameraOnTile(player.Tile.X, player.Tile.Y);

                TextArea text = StatusBar.FindChild("aps", false) as TextArea;
                text.Text = player.APs.ToString();
                text = StatusBar.FindChild("turn", false) as TextArea;
                text.Text = Turn.ToString();
                text = StatusBar.FindChild("depth", false) as TextArea;
                text.Text = String.Format("{0} / {1}", DungeonDepth, MaxDepth);
                text = StatusBar.FindChild("npc", false) as TextArea;
                text.Text = NpcList.Length.ToString(); 

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
        public void ExitToTitle(bool save = true)
        {
            if (save && level != null)
            {
                try
                {
                    using (FileStream stream = new FileStream(Path.Combine(Environment.CurrentDirectory, "savegame.rrs"), FileMode.Create, FileAccess.Write))
                    {
                        using (BinaryWriter writer = new BinaryWriter(stream, System.Text.Encoding.UTF8))
                        {
                            //player
                            writer.Write(Turn);
                            writer.Write(player.Type);
                            writer.Write((byte)player.State);
                            writer.Write(player.HP);
                            writer.Write(player.APs);
                            writer.Write(player.Tile.X);
                            writer.Write(player.Tile.Y);
                            for (int i = 0; i < player.Inventory.Length; i++)
                            {
                                Item item = player.Inventory[i];
                                if (item == null)
                                {
                                    writer.Write((Int16)(-1));
                                    continue;
                                }
                                Equipment eq = item as Equipment;
                                Weapon weap = item as Weapon;
                                Armor armor = item as Armor;
                                if (weap != null)
                                {
                                    writer.Write((Int16)1);
                                    writer.Write(item.gID);
                                    writer.Write((byte)weap.BodyType);
                                    writer.Write((byte)weap.AttackType);
                                    writer.Write(weap.Damage);
                                    writer.Write(weap.Range);
                                }
                                else if (armor != null)
                                {
                                    writer.Write((Int16)2);
                                    writer.Write(item.gID);
                                    writer.Write((byte)armor.BodyType);
                                    writer.Write(armor.Defense[0]);
                                    writer.Write(armor.Defense[1]);
                                    writer.Write(armor.Defense[2]);
                                }
                                else if (eq != null)
                                {
                                    writer.Write((Int16)3);
                                    writer.Write(item.gID);
                                    writer.Write((byte)eq.BodyType);
                                }
                                else
                                {
                                    writer.Write((Int16)0);
                                    writer.Write(item.gID);
                                }
                                writer.Write(item.Name);
                            }
                            for (int i = 0; i < player.Equipped.Length; i++)
                            {
                                Item item = player.Equipped[i];
                                if (item == null)
                                {
                                    writer.Write((Int16)(-1));
                                    continue;
                                }
                                Equipment eq = item as Equipment;
                                Weapon weap = item as Weapon;
                                Armor armor = item as Armor;
                                if (weap != null)
                                {
                                    writer.Write((Int16)1);
                                    writer.Write(item.gID);
                                    writer.Write((byte)weap.BodyType);
                                    writer.Write((byte)weap.AttackType);
                                    writer.Write(weap.Damage);
                                    writer.Write(weap.Range);
                                }
                                else if (armor != null)
                                {
                                    writer.Write((Int16)2);
                                    writer.Write(item.gID);
                                    writer.Write((byte)armor.BodyType);
                                    writer.Write(armor.Defense[0]);
                                    writer.Write(armor.Defense[1]);
                                    writer.Write(armor.Defense[2]);
                                }
                                else if (eq != null)
                                {
                                    writer.Write((Int16)3);
                                    writer.Write(item.gID);
                                    writer.Write((byte)eq.BodyType);
                                }
                                else
                                {
                                    writer.Write((Int16)0);
                                    writer.Write(item.gID);
                                }
                                writer.Write(item.Name);
                            }

                            //level
                            writer.Write(level.width);
                            writer.Write(level.height);
                            writer.Write(DungeonDepth);
                            writer.Write(MaxDepth);
                            writer.Write(level.layers.Length);
                            for (int i = 0; i < level.layers.Length; i++)
                            {
                                Layer layer = level.layers[i];
                                writer.Write((byte)layer.type);
                                for (int j = 0; j < layer.gIDs.Length; j++)
                                    writer.Write(layer.gIDs[j]);
                            }
                            //potion positions
                            for (int i = 0; i < potionPos.Length; i++)
                                writer.Write(potionPos[i]);
                            //uncovered
                            for (int i = 0; i < uncovered.Length; i++)
                                writer.Write(uncovered[i]);
                            //special tiles
                            for (int i = 0; i < specialTiles.Length; i++)
                                writer.Write(specialTiles[i]);

                            //npcs
                            List<NPC> activenpcs = new List<NPC>();
                            foreach (NPC npc in NpcList)
                            {
                                if (npc == null)
                                    continue;
                                activenpcs.Add(npc);
                            }
                            writer.Write(activenpcs.Count);
                            foreach (NPC npc in activenpcs)
                            {
                                writer.Write(npc.Tile.X);
                                writer.Write(npc.Tile.Y);
                                writer.Write(npc.Type);
                                writer.Write(npc.HP);
                                writer.Write(npc.APs);
                                writer.Write(npc.MaxAP);
                                writer.Write(npc.SeenPlayer);
                                writer.Write((byte)npc.State);
                                for (int i = 0; i < npc.Equipped.Length; i++)
                                {
                                    Item item = npc.Equipped[i];
                                    if (item == null)
                                    {
                                        writer.Write((Int16)(-1));
                                        continue;
                                    }
                                    Equipment eq = item as Equipment;
                                    Weapon weap = item as Weapon;
                                    Armor armor = item as Armor;
                                    if (weap != null)
                                    {
                                        writer.Write((Int16)1);
                                        writer.Write(item.gID);
                                        writer.Write((byte)weap.BodyType);
                                        writer.Write((byte)weap.AttackType);
                                        writer.Write(weap.Damage);
                                        writer.Write(weap.Range);
                                    }
                                    else if (armor != null)
                                    {
                                        writer.Write((Int16)2);
                                        writer.Write(item.gID);
                                        writer.Write((byte)armor.BodyType);
                                        writer.Write(armor.Defense[0]);
                                        writer.Write(armor.Defense[1]);
                                        writer.Write(armor.Defense[2]);
                                    }
                                    else if (eq != null)
                                    {
                                        writer.Write((Int16)3);
                                        writer.Write(item.gID);
                                        writer.Write((byte)eq.BodyType);
                                    }
                                    else
                                    {
                                        writer.Write((Int16)0);
                                        writer.Write(item.gID);
                                    }
                                    writer.Write(item.Name);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            }
            Game.Components.Add(new TitleScreen(Game));
            Game.Components.Remove(this);
            this.Dispose();
        }
    }
}
