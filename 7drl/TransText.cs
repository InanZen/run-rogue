﻿using Microsoft.Xna.Framework;
using System;

namespace RunRogue
{
    class TransText
    {
        public String Text;
        public Color Color;
        public float TransAmt = 0;
        public int TransMS = 500;
        public Vector2 StartPos;
        public Vector2 EndPos;
        public Vector2 Position
        {
            get
            {
                return Vector2.Lerp(StartPos, EndPos, TransAmt);
            }
        }
        public TransText(String text, Color c, Vector2 start, Vector2 end, int timeMS)
        {
            Text = text;
            Color = c;
            TransMS = timeMS;
            StartPos = start;
            EndPos = end;
        }
        public void Update(GameTime gameTime)
        {
            if (TransAmt != 1f)
            {
                float dT = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                TransAmt += (1f / 500) * dT;
                if (TransAmt > 1f)
                    TransAmt = 1f;
            }
        }
    }
}
