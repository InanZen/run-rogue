﻿using System;

namespace RunRogue
{
    class Item
    {
        public UInt16 gID;
        public String Name;
    }
    class Equipment : Item
    {
        public BodyPart BodyType;
    }
    class Weapon : Equipment
    {
        public byte Damage;
        public byte Range;
        public AttackType AttackType;
    }
    class Armor : Equipment
    {
        /// <summary>
        /// 0 = melee, 1 = ranged, 2 = magic
        /// </summary>
        public byte[] Defense = new byte[3];
    }
}
