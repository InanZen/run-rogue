﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace RunRogue
{
    public class Level
    {
        public string FileName;
        public string Name;
        public short width;
        public short height;
        public Layer[] layers;
        public List<Layer> GetLayersByType(LayerType type)
        {
            List<Layer> returnList = new List<Layer>();
            foreach (Layer l in layers)
                if (l.type == type)
                    returnList.Add(l);
            return returnList;
        }
        public BitArray GetCollisionBitArr()
        {
            BitArray ba = new BitArray(width * height);
            for (int i = 0; i < layers.Length; i++)
                if (layers[i].type == LayerType.Collision)
                    ba = ba.Or(layers[i].gIDs.ToBitArray());
            return ba;
        }
        public TileCollection[] tileSets;
        public TileCollection getTileSetFromGID(int gID)
        {
            for (int i = tileSets.Length - 1; i >= 0; i--)
                if (gID >= tileSets[i].firstGID)
                    return tileSets[i];
            return null;
        }
    }
    public class Layer
    {
        public LayerType type;
        public UInt16[] gIDs;
    }
    public enum LayerType
    {
        Animated = 0,
        Floor = 1,
        Scenery = 2,
        Collision = 3,
        Floating = 4
    }
}
