﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _7drl
{
    enum NpcState
    {
        Waiting = 0,
        Update = 1,
        Animation = 2
    }
    class NPC
    {
        public byte Type;
        public byte Direction = 2;
        public Vector2 Position = new Vector2();
        public Point Tile;
        public byte SpriteFrameIndex { get { return frameIDs[curFrame]; } }
        public byte curFrame;
        public byte[] frameIDs;
        public NpcState State;
        public float TotalTime = 0;
        public float FrameTime = 150f;
        public byte HP;
        public byte MaxAP;
        public byte APs;

        public NPC()
        {
            frameIDs = new byte[] { 0, 1, 2, 1 };
            curFrame = 1;
            State = NpcState.Waiting;
        }
        public virtual void Update(GameTime gameTime)
        {
            UpdateAnimation(gameTime);
        }
        public void UpdateAnimation(GameTime gameTime)
        {
            float sinceLast = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            TotalTime += sinceLast;
            if (TotalTime >= FrameTime)
            {
                curFrame++;
                curFrame = (byte)(curFrame % frameIDs.Length);
                TotalTime -= FrameTime;
            }
        }
    }
    class Player : NPC
    {
        public String Name;
        public Player(string name)
        {
            Name = name;
            frameIDs = new byte[] { 0, 1, 2, 1 };
            curFrame = 1;
            MaxAP = 4;
            APs = 4;
        }
        public override void Update(GameTime gametime)
        {
            base.Update(gametime);
        }
    }
}
